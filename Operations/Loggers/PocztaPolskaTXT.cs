﻿using ConfigurationManagement;
using System;
using System.IO;

namespace Operations
{
    internal class PocztaPolskaTXT
    {
        private string _documentName;

        public PocztaPolskaTXT()
        {
            _documentName = FileConfigurator.GetLocationOfStandardFiles();
            _documentName = _documentName +  "\\Paczki" + DateTime.Now.ToString("dd/MM/yyyy") + ".txt";
        }

        internal void LogPackageInfo(string info)
        {
            using (StreamWriter sw = File.AppendText(_documentName))
            {
                sw.WriteLine(info);
            }
        }
    }
}
