﻿using ConfigurationManagement;
using DatabaseConnection.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operations.CSV
{
    internal class AmbroCSV : BaseCSV
    {
        internal void CreateCsvForAmbro(int delivererId, int productId, DateTime forDate)
        {
            var factory = new TransactionFactory();
            _customerTransactionInfo = factory.GetTransactionsWithCustomerAddres(productId, delivererId, forDate);
            _sender = FileConfigurator.GetCompanyInfo();
            StringBuilder csvBuilder = new StringBuilder();

            csvBuilder.AppendLine("Odbiorca;Imię i Nazwisko;Telefon;Email;Adres*;Miasto*;Kod pocztowy*;Kraj;Liczba paczek*;Waga paczki" +
                "*;Pobranie;Ubezpieczenie;Uwagi;Referencja");

            foreach (var info in _customerTransactionInfo)
            {
                var moneyOnDeliver = info.PayOnDelivery ? Math.Round(info.PaymentOnDelivery, 2).ToString() : string.Empty;
                csvBuilder.AppendLine($"{info.CompanyName};{info.FirstName + " " + info.LastName};{info.Phone};{info.Email};{info.Street};" +
                    $"{info.City};{info.Zipcode};PL;;;{moneyOnDeliver};;;{info.ReferenceNumber};");
            }

            var path = $"\\ambro{DateTime.Now.ToString("dd/MM/yyyy")}.csv";
            SaveToFile(csvBuilder, path);
        }
    }
}
