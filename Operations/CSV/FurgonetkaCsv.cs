﻿using ConfigurationManagement;
using DatabaseConnection.Factories;
using DatabaseConnection.Models.OwnModels;
using Operations.CSV;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operations
{
    internal class FurgonetkaCsv : BaseCSV
    {


        internal void CreateCsvForFurgonetka(int delivererId, int productId, DateTime forDate)
        {
            var factory = new TransactionFactory();
            _customerTransactionInfo = factory.GetTransactionsWithCustomerAddres(productId, delivererId, forDate);
            _sender = FileConfigurator.GetCompanyInfo();
            StringBuilder csvBuilder = new StringBuilder();

            csvBuilder.AppendLine("service;sender_name;sender_surname;sender_company;sender_email;sender_street;sender_postcode;sender_city;sender_phone;sender_point;" +
                "receiver_type;receiver_name;receiver_surname;receiver_company;receiver_email;receiver_street;receiver_postcode;receiver_city;receiver_phone;" +
                "user_reference_number;type;shape;weight;width;height;depth;value;description;cod;iban");

            foreach (var info in _customerTransactionInfo)
            {
                var reciever_type = info.Reciver_type == DatabaseConnection.Other.Enums.Reciver_types.company ? "company" : "private";
                var iban = info.PayOnDelivery ? _sender.BankAccountNumber : string.Empty;
                var moneyOnDeliver = info.PayOnDelivery ? Math.Round(info.PaymentOnDelivery, 2).ToString() : string.Empty;
                var purchaseValue = Math.Round(info.Payment, 2).ToString();
                csvBuilder.AppendLine($"geis;{_sender.OwnerName};{_sender.OwnerSurname};{_sender.CompanyName};{_sender.CompanyEmail};{_sender.Street};{_sender.Postcode};{_sender.City};{_sender.Phone};;" +
                    $"{reciever_type}; {info.FirstName};{info.LastName};{info.CompanyName};{info.Email};{info.Street};{info.Zipcode};{info.City};{info.Phone};" +
                    $"{info.ReferenceNumber};package;;;;;;{purchaseValue};;{moneyOnDeliver};{iban};");
            }

            var path = $"\\furgonetka{DateTime.Now.ToString("dd/MM/yyyy")}.csv";
            SaveToFile(csvBuilder, path);
        }
    }

}
