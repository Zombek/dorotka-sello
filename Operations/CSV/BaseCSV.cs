﻿using ConfigurationManagement;
using DatabaseConnection.Models.OwnModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operations.CSV
{
    internal class BaseCSV
    {
        protected List<CustomerTransactionInfo> _customerTransactionInfo;
        protected CompanyInfo _sender;

        public void SaveToFile(StringBuilder toSave, string path)
        {
            string _documentName = FileConfigurator.GetLocationOfStandardFiles();
            _documentName = _documentName + path;

            using (StreamWriter sw = File.AppendText(_documentName))
            {
                sw.Write(toSave);
            }
        }
    }
}
