﻿using ConfigurationManagement;
using Operations.CSV;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operations
{
    public class CsvGenerator
    {
        public void GenerateCsvForCourier(string delivererName, int delivererId, int productId, DateTime forDate)
        {
            if (delivererName.Contains("Ambro"))
            {
                CreateCsvForAmbro(productId, delivererId, forDate);
            }
            else if (delivererName.Contains("Geis"))
            {
                CreateCsvForFurgonetka(productId, delivererId, forDate);
            }
        }

        private void CreateCsvForFurgonetka(int delivererId, int productId, DateTime forDate)
        {
           new FurgonetkaCsv().CreateCsvForFurgonetka(delivererId, productId, forDate);
        }

        private void CreateCsvForAmbro(int delivererId, int productId, DateTime forDate)
        {
            new AmbroCSV().CreateCsvForAmbro( delivererId,  productId , forDate);
        }

    }
}
