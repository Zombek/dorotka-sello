﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Operations
{
    public class CheckPocztaPolska
    {
        public int CheckedPackages = 0;

        public bool CheckThesePackages(List<string> numbers)
        {
            object locker = new Object();
            Parallel.ForEach(numbers, number =>
            {
                var checkedInfo = CheckThatPackage(number);
                if (!checkedInfo)
                {
                    lock (locker)
                    {
                        new PocztaPolskaTXT().LogPackageInfo($"Paczki o numerze {number} NIEDORĘCZONO");
                    }
                }
            });
            return true;
        }

        private bool CheckThatPackage(string packageNumber)
        {
            string url = "http://mobilna.poczta-polska.pl/MobiPost/getpackage?action=getPackageData&search=" + packageNumber;

            WebRequest request = WebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();

            string responseFromServer;
            using (Stream dataStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(dataStream);
                responseFromServer = reader.ReadToEnd();
            }

            responseFromServer = responseFromServer.Remove(responseFromServer.IndexOf("["), 1);
            responseFromServer = responseFromServer.Remove(responseFromServer.LastIndexOf("]"), 1);
            var package = JsonConvert.DeserializeObject<PocztaPackage>(responseFromServer);

            if (package.zdarzenia.Any(s => s.nazwa == "Doręczenie"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}


