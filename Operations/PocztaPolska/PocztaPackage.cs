﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operations
{
    internal class PocztaPackage
    {
        public string dataNadania { get; set; }
        public string kodKrajuNadania { get; set; }
        public string kodKrajuPrzezn { get; set; }
        public string kodRodzPrzes { get; set; }
        public string krajNadania { get; set; }
        public string krajPrzezn { get; set; }
        public string masa { get; set; }
        public string numer { get; set; }
        public string rodzPrzes { get; set; }
        public string jednstkaNadania { get; set; }
        public string jednstkaPrzeznaczenia { get; set; }
        public List<PackageStatus> zdarzenia { get; set; }

        public PocztaPackage()
        {
            zdarzenia = new List<PackageStatus>();
        }
    }
}
