﻿namespace Operations
{
    public class PackageStatus
    {
        public string czasZadrzenia { get; set; }
        public string daneSzczegJednostki { get; set; }
        public string nazwa { get; set; }
        public string przyczyna { get; set; }
    }
}