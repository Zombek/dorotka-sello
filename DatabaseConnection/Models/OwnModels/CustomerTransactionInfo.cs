﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DatabaseConnection.Other.Enums;

namespace DatabaseConnection.Models.OwnModels
{
    public class CustomerTransactionInfo
    {
        public bool PayOnDelivery { get; private set; }
        public Reciver_types Reciver_type { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string CompanyName { get; private set; }
        public string Email { get; private set; }
        public string Street { get; private set; }
        public string Zipcode { get; private set; }
        public string City { get; private set; }
        public string Phone { get; private set; }
        public string ReferenceNumber { get; private set; }
        public decimal Payment { get; private set; }
        public decimal PaymentOnDelivery { get; private set; }


        private CustomerTransactionInfo()
        {
        }

        public CustomerTransactionInfo(bool payOnDelivery, string reciver_type, string customerName, string companyName, string email, 
                                        string street, string zipcode, string city, string phone, string referenceNumber, decimal payment,
                                        decimal paymentOnDelivery)
        {
            PayOnDelivery = payOnDelivery;
            Reciver_type = string.IsNullOrEmpty(reciver_type) ? Reciver_types.private_customer : Reciver_types.company;
            FirstName = customerName.Split(' ')[0];
            LastName = customerName.Split(' ')[1]; 
            CompanyName = companyName;
            Email = email;
            Street = street;
            Zipcode = zipcode;
            City = city;
            Phone = phone;
            ReferenceNumber = referenceNumber;
            Payment = payment;
            PaymentOnDelivery = paymentOnDelivery;
        }
    }
}
