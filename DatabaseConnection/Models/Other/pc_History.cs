namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pc_History
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ph_Id { get; set; }

        public int ph_PackageId { get; set; }

        public DateTime ph_Time { get; set; }

        public int ph_UserId { get; set; }

        [Required]
        [StringLength(255)]
        public string ph_Entry { get; set; }

        public virtual pc__Package pc__Package { get; set; }
    }
}
