namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class reg__Registration
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public reg__Registration()
        {
            au__Auction = new HashSet<au__Auction>();
            pc__Package = new HashSet<pc__Package>();
            reg_AfterSalesServiceConditions = new HashSet<reg_AfterSalesServiceConditions>();
            reg_Data = new HashSet<reg_Data>();
            reg_JournalDeals = new HashSet<reg_JournalDeals>();
            reg_Site = new HashSet<reg_Site>();
            st_Category = new HashSet<st_Category>();
            tr__Transaction = new HashSet<tr__Transaction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int reg_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string reg_Username { get; set; }

        [Required]
        [StringLength(2500)]
        public string reg_Accesskey { get; set; }

        [Required]
        [StringLength(50)]
        public string reg_Description { get; set; }

        [Required]
        [StringLength(50)]
        public string reg_Version { get; set; }

        public int reg_SvcProvId { get; set; }

        [Required]
        [StringLength(255)]
        public string reg_SvcUserId { get; set; }

        [Required]
        [StringLength(50)]
        public string reg_Name { get; set; }

        [Required]
        [StringLength(50)]
        public string reg_FullName { get; set; }

        public int? reg_EmailId { get; set; }

        public bool reg_Active { get; set; }

        public bool reg_ST_StatusesServiceActive { get; set; }

        public bool reg_ST_ShowDocumentAddressForm { get; set; }

        public bool reg_ST_ShowDeliveryAddressForm { get; set; }

        [Required]
        [StringLength(500)]
        public string reg_ST_Description { get; set; }

        [Required]
        [StringLength(255)]
        public string reg_ST_WWW { get; set; }

        public DateTime? reg_ST_LastSynch { get; set; }

        public bool reg_HasShop { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reg_AfterSalesServiceConditions> reg_AfterSalesServiceConditions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reg_Data> reg_Data { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reg_JournalDeals> reg_JournalDeals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reg_Site> reg_Site { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<st_Category> st_Category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr__Transaction> tr__Transaction { get; set; }
    }
}
