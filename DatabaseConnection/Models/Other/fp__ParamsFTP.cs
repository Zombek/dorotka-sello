namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class fp__ParamsFTP
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int fp_Id { get; set; }

        [StringLength(500)]
        public string fp_ServerURLFTP { get; set; }

        [StringLength(500)]
        public string fp_ServerPathFTP { get; set; }

        [StringLength(100)]
        public string fp_UsernameFTP { get; set; }

        [StringLength(100)]
        public string fp_PasswordFTP { get; set; }

        [StringLength(500)]
        public string fp_ServerURLHTTP { get; set; }

        [StringLength(500)]
        public string fp_ServerPathHTTP { get; set; }

        [StringLength(100)]
        public string fp_ProxyUser { get; set; }

        [StringLength(100)]
        public string fp_ProxyPassword { get; set; }

        [StringLength(100)]
        public string fp_ProxyDomain { get; set; }

        public bool? fp_ProxyNeedsAuth { get; set; }

        public bool? fp_PassiveMode { get; set; }

        public bool fp_UseFTP { get; set; }
    }
}
