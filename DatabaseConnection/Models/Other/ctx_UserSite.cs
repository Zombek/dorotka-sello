namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ctx_UserSite
    {
        [Key]
        public int cus_Id { get; set; }

        public int cus_UserId { get; set; }

        public int cus_RegSiteId { get; set; }

        public virtual reg_Site reg_Site { get; set; }

        public virtual sl_User sl_User { get; set; }
    }
}
