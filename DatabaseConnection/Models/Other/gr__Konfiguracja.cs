namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class gr__Konfiguracja
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string cg_Section { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cg_User { get; set; }

        [StringLength(50)]
        public string cg_Version { get; set; }

        [Column(TypeName = "image")]
        public byte[] cg_BinListInfo { get; set; }

        [Column(TypeName = "image")]
        public byte[] cg_BinColumns { get; set; }

        [Column(TypeName = "image")]
        public byte[] cg_BinTables { get; set; }

        [Column(TypeName = "image")]
        public byte[] cg_BinVisibility { get; set; }
    }
}
