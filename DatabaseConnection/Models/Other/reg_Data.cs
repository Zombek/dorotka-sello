namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class reg_Data
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int rd_Id { get; set; }

        public int rd_RegId { get; set; }

        [Required]
        [StringLength(50)]
        public string rd_Name { get; set; }

        [StringLength(3500)]
        public string rd_vvarchar { get; set; }

        public int? rd_vint { get; set; }

        [Column(TypeName = "money")]
        public decimal? rd_vmoney { get; set; }

        public DateTime? rd_vdatetime { get; set; }

        [MaxLength(3500)]
        public byte[] rd_vbinary { get; set; }

        public long? rd_vbigint { get; set; }

        public virtual reg__Registration reg__Registration { get; set; }
    }
}
