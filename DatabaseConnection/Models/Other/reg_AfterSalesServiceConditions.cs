namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class reg_AfterSalesServiceConditions
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int assc_Id { get; set; }

        public int assc_RegId { get; set; }

        public int assc_Type { get; set; }

        [Required]
        [StringLength(64)]
        public string assc_ExternalId { get; set; }

        [Required]
        [StringLength(100)]
        public string assc_Name { get; set; }

        public virtual reg__Registration reg__Registration { get; set; }
    }
}
