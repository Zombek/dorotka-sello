namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_TransactionProperty
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int atp_Id { get; set; }

        public int atp_AuctionId { get; set; }

        public int atp_TransactionPropertyId { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual sl_TransProperty sl_TransProperty { get; set; }
    }
}
