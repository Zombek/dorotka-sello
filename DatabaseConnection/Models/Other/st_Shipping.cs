namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_Shipping
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_Shipping()
        {
            au_Shipping = new HashSet<au_Shipping>();
            pl_PricingListData = new HashSet<pl_PricingListData>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ss_Id { get; set; }

        public int ss_SvcProvId { get; set; }

        public int ss_SiteId { get; set; }

        [Required]
        [StringLength(255)]
        public string ss_Name { get; set; }

        public int? ss_Ident { get; set; }

        [StringLength(100)]
        public string ss_Code { get; set; }

        public bool? ss_Expired { get; set; }

        public bool? ss_Intl { get; set; }

        public bool? ss_Show { get; set; }

        public int? ss_Fid { get; set; }

        public int? ss_FieldType { get; set; }

        public int ss_Group { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Shipping> au_Shipping { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pl_PricingListData> pl_PricingListData { get; set; }
    }
}
