namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class up_UserParams
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int up_Id { get; set; }

        public int up_UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string up_Name { get; set; }

        [StringLength(3500)]
        public string up_vvarchar { get; set; }

        public int? up_vint { get; set; }

        [Column(TypeName = "money")]
        public decimal? up_vmoney { get; set; }

        public DateTime? up_vdatetime { get; set; }

        [MaxLength(3500)]
        public byte[] up_vbinary { get; set; }

        public bool? up_vbit { get; set; }
    }
}
