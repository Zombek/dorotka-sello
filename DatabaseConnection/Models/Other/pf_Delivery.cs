namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pf_Delivery
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pd_Id { get; set; }

        public int pd_DeliveryId { get; set; }

        public int pd_PrintFormId { get; set; }

        public virtual pf_PrintForm pf_PrintForm { get; set; }

        public virtual sl_Delivery sl_Delivery { get; set; }
    }
}
