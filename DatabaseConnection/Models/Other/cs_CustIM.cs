namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_CustIM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cm_Id { get; set; }

        public int cm_CustomerId { get; set; }

        public int cm_IMTypeId { get; set; }

        [Required]
        [StringLength(128)]
        public string cm_Info { get; set; }

        public virtual cs__Customer cs__Customer { get; set; }

        public virtual sl_InstantMessenger sl_InstantMessenger { get; set; }
    }
}
