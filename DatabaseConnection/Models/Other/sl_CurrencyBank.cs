namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_CurrencyBank
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_CurrencyBank()
        {
            it_ItemSite = new HashSet<it_ItemSite>();
            sl_CurrencyExchangeRates = new HashSet<sl_CurrencyExchangeRates>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cb_ID { get; set; }

        [Required]
        [StringLength(50)]
        public string cb_Name { get; set; }

        public bool cb_Default { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_ItemSite> it_ItemSite { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sl_CurrencyExchangeRates> sl_CurrencyExchangeRates { get; set; }
    }
}
