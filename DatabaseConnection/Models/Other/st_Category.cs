namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_Category
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_Category()
        {
            au__Auction = new HashSet<au__Auction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cat_Id { get; set; }

        public int cat_SiteId { get; set; }

        public int cat_CatId { get; set; }

        public int cat_ParentId { get; set; }

        [Required]
        [StringLength(100)]
        public string cat_Name { get; set; }

        public bool? cat_Leaf { get; set; }

        public bool cat_Expired { get; set; }

        public bool? cat_Virtual { get; set; }

        public int? cat_Position { get; set; }

        public int? cat_RegId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }

        public virtual reg__Registration reg__Registration { get; set; }

        public virtual st__Site st__Site { get; set; }
    }
}
