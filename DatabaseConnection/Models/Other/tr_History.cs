namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tr_History
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int th_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int th_TransId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime th_Time { get; set; }

        public int? th_UserId { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(255)]
        public string th_Entry { get; set; }
    }
}
