namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em__Email
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public em__Email()
        {
            em_Attachment = new HashSet<em_Attachment>();
            em_Source1 = new HashSet<em_Source>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int em_Id { get; set; }

        public DateTime em_Date { get; set; }

        [Required]
        [StringLength(255)]
        public string em_UIDL { get; set; }

        [Required]
        [StringLength(255)]
        public string em_MessageId { get; set; }

        [Required]
        [StringLength(1000)]
        public string em_Subject { get; set; }

        [Required]
        [StringLength(255)]
        public string em_From { get; set; }

        [Required]
        [StringLength(255)]
        public string em_FromAddress { get; set; }

        [Required]
        [StringLength(255)]
        public string em_FromName { get; set; }

        [Required]
        [StringLength(1000)]
        public string em_To { get; set; }

        public DateTime? em_Received { get; set; }

        public DateTime? em_Sent { get; set; }

        [Required]
        [StringLength(50)]
        public string em_Account { get; set; }

        public int? em_AttachmentCount { get; set; }

        [Column(TypeName = "text")]
        public string em_Source { get; set; }

        public bool? em_HTMLBody { get; set; }

        public int em_Status { get; set; }

        public int? em_AccountId { get; set; }

        public int? em_TransId { get; set; }

        public int? em_PackageId { get; set; }

        public int? em_AuctionId { get; set; }

        public int? em_CustomerId { get; set; }

        public int? em_PrevTransId { get; set; }

        public bool em_Handled { get; set; }

        public bool em_Read { get; set; }

        public int? em_GroupId { get; set; }

        public int em_Flag { get; set; }

        [StringLength(255)]
        public string em_ReplyTo { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual cs__Customer cs__Customer { get; set; }

        public virtual pc__Package pc__Package { get; set; }

        public virtual sl_EmailGroup sl_EmailGroup { get; set; }

        public virtual tr__Transaction tr__Transaction { get; set; }

        public virtual tr__Transaction tr__Transaction1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em_Attachment> em_Attachment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em_Source> em_Source1 { get; set; }
    }
}
