namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st__Site
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st__Site()
        {
            au__Auction = new HashSet<au__Auction>();
            it_ItemSite = new HashSet<it_ItemSite>();
            reg_JournalDeals = new HashSet<reg_JournalDeals>();
            reg_Site = new HashSet<reg_Site>();
            st_Category = new HashSet<st_Category>();
            st_SiteData = new HashSet<st_SiteData>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int st_Id { get; set; }

        public int st_SvcProvId { get; set; }

        public int st_SiteId { get; set; }

        [StringLength(20)]
        public string st_CategoriesVersion { get; set; }

        [StringLength(20)]
        public string st_FormVersion { get; set; }

        [StringLength(20)]
        public string st_AttributesVersion { get; set; }

        [Required]
        [StringLength(3)]
        public string st_Currency { get; set; }

        public DateTime? st_Checked { get; set; }

        public bool st_UpdateNeeded { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_ItemSite> it_ItemSite { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reg_JournalDeals> reg_JournalDeals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reg_Site> reg_Site { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<st_Category> st_Category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<st_SiteData> st_SiteData { get; set; }
    }
}
