namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class it_ItemProp
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ip_Id { get; set; }

        public int ip_ItemId { get; set; }

        public int ip_PropertyId { get; set; }

        public virtual it__Item it__Item { get; set; }

        public virtual sl_ItemProperty sl_ItemProperty { get; set; }
    }
}
