namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em_Archive
    {
        [Key]
        public int ear_Id { get; set; }

        [Required]
        [StringLength(150)]
        public string ear_UIDL { get; set; }

        public int? ear_AccountId { get; set; }
    }
}
