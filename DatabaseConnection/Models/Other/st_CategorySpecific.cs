namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_CategorySpecific
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_CategorySpecific()
        {
            st_CategorySpecificDict = new HashSet<st_CategorySpecificDict>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cs_Id { get; set; }

        public int cs_SvcProvId { get; set; }

        public int cs_SiteId { get; set; }

        public int? cs_LeafCategoryNumber { get; set; }

        public int cs_ItemId { get; set; }

        [Required]
        [StringLength(100)]
        public string cs_Title { get; set; }

        public int cs_CatId { get; set; }

        public int cs_InputType { get; set; }

        public int cs_DataType { get; set; }

        public int cs_Default { get; set; }

        public int cs_Required { get; set; }

        public int cs_Position { get; set; }

        public int cs_Length { get; set; }

        [StringLength(100)]
        public string cs_MinVal { get; set; }

        [Required]
        [StringLength(100)]
        public string cs_MaxVal { get; set; }

        [StringLength(1000)]
        public string cs_Description { get; set; }

        [Required]
        [StringLength(255)]
        public string cs_Values { get; set; }

        [Column(TypeName = "ntext")]
        public string cs_Help { get; set; }

        public int? cs_SellFormParentId { get; set; }

        [StringLength(100)]
        public string cs_SellFormParentValue { get; set; }

        [StringLength(100)]
        public string cs_SellFormUnit { get; set; }

        public int? cs_SellFormOptions { get; set; }

        public int cs_SellFormParamId { get; set; }

        public bool cs_IsVariantParameter { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<st_CategorySpecificDict> st_CategorySpecificDict { get; set; }
    }
}
