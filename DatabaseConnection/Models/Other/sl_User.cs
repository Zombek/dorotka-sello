namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_User()
        {
            au__Auction = new HashSet<au__Auction>();
            cs__Customer = new HashSet<cs__Customer>();
            ctx_UserSite = new HashSet<ctx_UserSite>();
            em_Account = new HashSet<em_Account>();
            ep_UserMapping = new HashSet<ep_UserMapping>();
            jb__Job = new HashSet<jb__Job>();
            nt_Note = new HashSet<nt_Note>();
            pc__Package = new HashSet<pc__Package>();
            pd_AutoSyncParams = new HashSet<pd_AutoSyncParams>();
            pd_UserPermission = new HashSet<pd_UserPermission>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int usr_Id { get; set; }

        [Required]
        [StringLength(64)]
        public string usr_Name { get; set; }

        [Required]
        [StringLength(64)]
        public string usr_SurName { get; set; }

        public int? usr_RegId { get; set; }

        public int? usr_RegSiteId { get; set; }

        [Required]
        [StringLength(100)]
        public string usr_Password { get; set; }

        public bool usr_Active { get; set; }

        public DateTime? usr_ShowNewsPageOnStartupDate { get; set; }

        [Required]
        [StringLength(50)]
        public string usr_Nick { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs__Customer> cs__Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ctx_UserSite> ctx_UserSite { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em_Account> em_Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ep_UserMapping> ep_UserMapping { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<jb__Job> jb__Job { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<nt_Note> nt_Note { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_AutoSyncParams> pd_AutoSyncParams { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_UserPermission> pd_UserPermission { get; set; }
    }
}
