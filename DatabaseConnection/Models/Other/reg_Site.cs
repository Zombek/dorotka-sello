namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class reg_Site
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public reg_Site()
        {
            ctx_UserSite = new HashSet<ctx_UserSite>();
            pc__Package = new HashSet<pc__Package>();
            tr__Transaction = new HashSet<tr__Transaction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int rs_Id { get; set; }

        public int rs_RegId { get; set; }

        public int rs_SiteId { get; set; }

        [Required]
        [StringLength(50)]
        public string rs_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ctx_UserSite> ctx_UserSite { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package { get; set; }

        public virtual reg__Registration reg__Registration { get; set; }

        public virtual st__Site st__Site { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr__Transaction> tr__Transaction { get; set; }
    }
}
