namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_PendingChanges
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int apc_Id { get; set; }

        public int apc_AuctionId { get; set; }

        public int apc_ChangeType { get; set; }

        [StringLength(100)]
        public string apc_ChangeText { get; set; }

        [StringLength(3500)]
        public string apc_vvarchar { get; set; }

        public int? apc_vint { get; set; }

        [Column(TypeName = "money")]
        public decimal? apc_vmoney { get; set; }

        public int apc_ChangeState { get; set; }

        [Required]
        [StringLength(1024)]
        public string apc_ResultInfo { get; set; }

        public DateTime? apc_SendDate { get; set; }

        public DateTime apc_ChangeDate { get; set; }

        [StringLength(50)]
        public string apc_ChangeGUID { get; set; }

        [Column(TypeName = "ntext")]
        public string apc_LongText { get; set; }

        public virtual au__Auction au__Auction { get; set; }
    }
}
