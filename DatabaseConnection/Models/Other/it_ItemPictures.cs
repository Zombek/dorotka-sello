namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class it_ItemPictures
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pc_Id { get; set; }

        public int pc_ItemId { get; set; }

        public int pc_PictureId { get; set; }

        public int pc_PictureOrder { get; set; }

        public virtual im__Image im__Image { get; set; }

        public virtual it__Item it__Item { get; set; }
    }
}
