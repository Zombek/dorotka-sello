namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_CustomerProp
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cp_Id { get; set; }

        public int cp_CustId { get; set; }

        public int cp_PropId { get; set; }

        public virtual cs__Customer cs__Customer { get; set; }

        public virtual sl_CustProperty sl_CustProperty { get; set; }
    }
}
