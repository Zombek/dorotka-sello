namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd_Sesja
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ss_spid { get; set; }

        public int ss_uid { get; set; }

        [Required]
        [StringLength(50)]
        public string ss_hid { get; set; }

        public int ss_apid { get; set; }

        public DateTime ss_data { get; set; }

        public bool ss_ignore { get; set; }

        public DateTime? ss_data_zamkniecia { get; set; }

        public DateTime? ss_data_blokady_rach { get; set; }
    }
}
