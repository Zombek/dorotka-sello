namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ds_DeliveryToShippingMap
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ds_Id { get; set; }

        public int ds_DeliveryId { get; set; }

        [Required]
        [StringLength(50)]
        public string ds_ServiceShippingId { get; set; }

        public virtual sl_Delivery sl_Delivery { get; set; }
    }
}
