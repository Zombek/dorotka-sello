namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class it_ItemSite
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int is_Id { get; set; }

        public int is_ItemId { get; set; }

        public int? is_SiteId { get; set; }

        [StringLength(100)]
        public string is_Name { get; set; }

        [Column(TypeName = "ntext")]
        public string is_Description { get; set; }

        public int? is_AutoCalcType { get; set; }

        [Column(TypeName = "money")]
        public decimal? is_Price { get; set; }

        [Column(TypeName = "money")]
        public decimal is_AC_ParamA_L { get; set; }

        [Column(TypeName = "money")]
        public decimal is_AC_ParamA_M { get; set; }

        [Column(TypeName = "money")]
        public decimal is_AC_ParamB { get; set; }

        public bool is_AC_UseExchangeRate { get; set; }

        public int is_AC_ExchangeRateType { get; set; }

        public int? is_AC_ExchangeRateBankId { get; set; }

        public bool is_AC_ModifyPrice { get; set; }

        public int is_AC_ModifyPriceType { get; set; }

        public virtual it__Item it__Item { get; set; }

        public virtual sl_CurrencyBank sl_CurrencyBank { get; set; }

        public virtual st__Site st__Site { get; set; }
    }
}
