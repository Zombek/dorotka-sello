namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_Shipping
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int as_Id { get; set; }

        public int as_AuctionId { get; set; }

        public int as_OptionId { get; set; }

        public int? as_LocationId { get; set; }

        [Column(TypeName = "money")]
        public decimal? as_Cost { get; set; }

        [Column(TypeName = "money")]
        public decimal? as_Cost2 { get; set; }

        public int? as_Priority { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual st_Shipping st_Shipping { get; set; }
    }
}
