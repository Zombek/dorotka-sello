namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_CustomGlobalAttributes
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cga_Id { get; set; }

        [Required]
        [StringLength(100)]
        public string cga_Name { get; set; }

        public bool cga_HTML { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string cga_AttributeValue { get; set; }
    }
}
