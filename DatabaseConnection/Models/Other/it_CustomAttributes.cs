namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class it_CustomAttributes
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ia_Id { get; set; }

        public int ia_ItemId { get; set; }

        public int ia_AttributeId { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string ia_AttributeValue { get; set; }

        public bool ia_ConvertToHtml { get; set; }

        public virtual it__Item it__Item { get; set; }

        public virtual sl_CustomAttributes sl_CustomAttributes { get; set; }
    }
}
