namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_CategorySpecific
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int as_Id { get; set; }

        public int as_AuctionId { get; set; }

        [Required]
        [StringLength(60)]
        public string as_FieldId { get; set; }

        public int? as_vint { get; set; }

        [Column(TypeName = "money")]
        public decimal? as_vmoney { get; set; }

        public DateTime? as_vdatetime { get; set; }

        [StringLength(512)]
        public string as_vvarchar { get; set; }

        public virtual au__Auction au__Auction { get; set; }
    }
}
