namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_Allegro
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int aal_Id { get; set; }

        public int aal_AuctionId { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string aal_Description { get; set; }

        public short aal_ShippingPayment { get; set; }

        [Required]
        [StringLength(500)]
        public string aal_AdditionalInfo { get; set; }

        public int aal_Unit { get; set; }

        [Required]
        [StringLength(35)]
        public string aal_BankAccount1 { get; set; }

        [Required]
        [StringLength(35)]
        public string aal_BankAccount2 { get; set; }

        public int? aal_FulfillmentTime { get; set; }

        [Required]
        [StringLength(36)]
        public string aal_ImpliedWarrantyServiceId { get; set; }

        [Required]
        [StringLength(36)]
        public string aal_ReturnPolicyServiceId { get; set; }

        [Required]
        [StringLength(36)]
        public string aal_WarrantyServiceId { get; set; }

        public virtual au__Auction au__Auction { get; set; }
    }
}
