namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pl_PricingListData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pd_Id { get; set; }

        public int pd_PricingListId { get; set; }

        public int pd_OptionId { get; set; }

        [Column(TypeName = "money")]
        public decimal? pd_Cost { get; set; }

        [Column(TypeName = "money")]
        public decimal? pd_Cost2 { get; set; }

        public int? pd_MaxItems { get; set; }

        public virtual sl_PricingList sl_PricingList { get; set; }

        public virtual st_Shipping st_Shipping { get; set; }
    }
}
