namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pc_SendingListPackage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pp_Id { get; set; }

        public int pp_ListId { get; set; }

        public int pp_PackageId { get; set; }

        public int? pp_SendingListNumber { get; set; }

        public virtual pc__Package pc__Package { get; set; }
    }
}
