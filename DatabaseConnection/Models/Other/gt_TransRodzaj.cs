namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class gt_TransRodzaj
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int gtr_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string gtr_Nazwa { get; set; }
    }
}
