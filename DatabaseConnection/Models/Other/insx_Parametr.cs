namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class insx_Parametr
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public insx_Parametr()
        {
            xcs_Szum = new HashSet<xcs_Szum>();
            xit_Szum = new HashSet<xit_Szum>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ixp_Id { get; set; }

        public bool ixp_TwWlaczInstynkt { get; set; }

        public int ixp_TwLiczbaPozycji { get; set; }

        public int ixp_TwStanMagazynowy { get; set; }

        public bool ixp_TwWarunekAnd { get; set; }

        public bool ixp_TwSortujAlfabetycznie { get; set; }

        public int ixp_TwMinimalnaLiczbaZnakow { get; set; }

        public bool ixp_KlWlaczInstynkt { get; set; }

        public int ixp_KlLiczbaPozycji { get; set; }

        public bool ixp_KlWarunekAnd { get; set; }

        public bool ixp_KlSortujAlfabetycznie { get; set; }

        public int ixp_KlMinimalnaLiczbaZnakow { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xcs_Szum> xcs_Szum { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xit_Szum> xit_Szum { get; set; }
    }
}
