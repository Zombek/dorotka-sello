namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_Ebay
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int aeb_Id { get; set; }

        public int aeb_AuctionId { get; set; }

        public int? aeb_Cat2Id { get; set; }

        [StringLength(100)]
        public string aeb_Subtitle { get; set; }

        public int? aeb_LotSize { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string aeb_Description { get; set; }

        public int? aeb_HitCounter { get; set; }

        [StringLength(50)]
        public string aeb_Phone1 { get; set; }

        [StringLength(50)]
        public string aeb_Phone2 { get; set; }

        public bool aeb_SkypeChat { get; set; }

        public bool aeb_SkypeVoice { get; set; }

        public int? aeb_InsuranceDomId { get; set; }

        public int? aeb_InsuranceIntlId { get; set; }

        [Column(TypeName = "money")]
        public decimal? aeb_InsuranceDomCost { get; set; }

        [Column(TypeName = "money")]
        public decimal? aeb_InsuranceIntlCost { get; set; }

        [Required]
        [StringLength(500)]
        public string aeb_AdditionalInfo { get; set; }

        public bool aeb_PromotionalDom { get; set; }

        public bool aeb_PromotionalIntl { get; set; }

        public int aeb_DispatchTime { get; set; }

        [Required]
        [StringLength(255)]
        public string aeb_PayPalEmail { get; set; }

        public bool aeb_PayPalImmediate { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual st_Insurance st_Insurance { get; set; }

        public virtual st_Insurance st_Insurance1 { get; set; }
    }
}
