namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd_BankAccounts
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pd_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string pd_BankName { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string pd_AccountNumber { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pd_EntityId { get; set; }

        public int? pd_DefaultNumber { get; set; }

        public virtual pd__Podmiot pd__Podmiot { get; set; }
    }
}
