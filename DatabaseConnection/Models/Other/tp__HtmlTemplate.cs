namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tp__HtmlTemplate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tp__HtmlTemplate()
        {
            au__Auction = new HashSet<au__Auction>();
            tp_Image = new HashSet<tp_Image>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tp_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string tp_Name { get; set; }

        [Column(TypeName = "ntext")]
        public string tp_Html { get; set; }

        public DateTime tp_DateAdded { get; set; }

        public DateTime tp_DateLastModified { get; set; }

        public int tp_AuthorId { get; set; }

        public int tp_LanguageId { get; set; }

        public bool tp_ReadOnly { get; set; }

        public bool tp_Active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }

        public virtual sl_Language sl_Language { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tp_Image> tp_Image { get; set; }
    }
}
