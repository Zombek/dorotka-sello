namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_Enhancement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_Enhancement()
        {
            au_Enhancement = new HashSet<au_Enhancement>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int se_Id { get; set; }

        public int se_SvcProvId { get; set; }

        public int se_SiteId { get; set; }

        [Required]
        [StringLength(255)]
        public string se_Name { get; set; }

        public int? se_Ident { get; set; }

        [StringLength(50)]
        public string se_Code { get; set; }

        public bool? se_Expired { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Enhancement> au_Enhancement { get; set; }
    }
}
