namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("__Modyfikacja")]
    public partial class C__Modyfikacja
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(50)]
        public string Kolumna { get; set; }

        [StringLength(50)]
        public string Tabela { get; set; }

        [StringLength(500)]
        public string Zrodlo { get; set; }
    }
}
