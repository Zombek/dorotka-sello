namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_Features
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int fe_Id { get; set; }

        public int fe_ObjectId { get; set; }

        public int fe_Code { get; set; }

        [StringLength(255)]
        public string fe_Comment { get; set; }

        public int? fe_Value { get; set; }

        public virtual au__Auction au__Auction { get; set; }
    }
}
