namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em_Rule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public em_Rule()
        {
            em_RuleWord = new HashSet<em_RuleWord>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int er_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string er_Name { get; set; }

        public int? er_GroupId { get; set; }

        public bool? er_MarkAsRead { get; set; }

        public int er_Operator { get; set; }

        public bool er_Enabled { get; set; }

        public int er_Rank { get; set; }

        public int er_Flag { get; set; }

        public virtual sl_EmailGroup sl_EmailGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em_RuleWord> em_RuleWord { get; set; }
    }
}
