namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class gt_TransObiekt
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int to_TransformacjaId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int to_DefinicjaId { get; set; }

        public int? to_UzytkownikId { get; set; }

        public virtual gt_Definicja gt_Definicja { get; set; }

        public virtual gt_Transformacja gt_Transformacja { get; set; }
    }
}
