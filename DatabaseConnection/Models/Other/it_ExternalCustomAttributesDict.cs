namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class it_ExternalCustomAttributesDict
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public it_ExternalCustomAttributesDict()
        {
            it_ExternalCustomAttributes = new HashSet<it_ExternalCustomAttributes>();
        }

        [Key]
        public int ecd_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string ecd_SelloName { get; set; }

        [Required]
        [StringLength(50)]
        public string ecd_ExternalName { get; set; }

        public bool ecd_IsStandard { get; set; }

        public int? ecd_StandardIndex { get; set; }

        public bool ecd_Invalidated { get; set; }

        public bool ecd_IsSQL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_ExternalCustomAttributes> it_ExternalCustomAttributes { get; set; }
    }
}
