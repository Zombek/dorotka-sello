namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_Currency
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cy_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string cy_Name { get; set; }

        [Required]
        [StringLength(3)]
        public string cy_Symbol { get; set; }

        [Required]
        [StringLength(50)]
        public string cy_Country { get; set; }

        [Column(TypeName = "money")]
        public decimal cy_Multiplier { get; set; }
    }
}
