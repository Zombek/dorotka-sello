namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pc_SendingList
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pl_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string pl_Symbol { get; set; }

        public DateTime pl_CreationDate { get; set; }

        public DateTime? pl_SendingDate { get; set; }

        public DateTime? pl_Printed { get; set; }

        public int? pl_DelivererId { get; set; }

        public virtual sl_Deliverer sl_Deliverer { get; set; }
    }
}
