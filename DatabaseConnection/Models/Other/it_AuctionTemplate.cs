namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class it_AuctionTemplate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ia_Id { get; set; }

        public int ia_ItemId { get; set; }

        public int ia_AuctionId { get; set; }

        public bool ia_Default { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual it__Item it__Item { get; set; }
    }
}
