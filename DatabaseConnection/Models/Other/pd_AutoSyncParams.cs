namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd_AutoSyncParams
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pa_Id { get; set; }

        public bool pa_AuctionsAutoSyncParams { get; set; }

        public bool pa_AuctionsReceiveDialog { get; set; }

        public bool pa_AuctionsSendDialog { get; set; }

        public bool pa_DefaultSaleReceive { get; set; }

        public bool pa_DefaultSaleReceiveFull { get; set; }

        public bool pa_DefaultAuctionReceiveSell { get; set; }

        public bool pa_DefaultAuctionReceiveSold { get; set; }

        public bool pa_DefaultAuctionReceiveNotSold { get; set; }

        public int pa_DefaultAuctionReceiveFullData { get; set; }

        public int pa_DefaultFeedbackReceiveOption { get; set; }

        public int pa_DefaultFeedbackReceiveLastCount { get; set; }

        public int pa_DefaultFeedbackReceiveNewestDaysCount { get; set; }

        public bool pa_DefaultFeedbackReceiveBuyersOnly { get; set; }

        public bool pa_DefaultAuctionSend { get; set; }

        public bool pa_DefaultAuctionUpdate { get; set; }

        public bool pa_DefaultFeedbackSend { get; set; }

        public int pa_AuctSyncSendImgMaxSize { get; set; }

        public int pa_AuctSyncSendImgMaxLen { get; set; }

        public bool pa_AuctSyncSendCurReg { get; set; }

        public bool pa_AuctSyncReceiveCurReg { get; set; }

        public bool pa_AuctSyncUseJournal { get; set; }

        public int? pa_ServerUserId { get; set; }

        public bool pa_AutoComment { get; set; }

        public bool pa_AuctSchedule { get; set; }

        public int pa_AuctScheduleRetryDuration { get; set; }

        public DateTime? pa_AuctSyncTransactionsCutDate { get; set; }

        public bool pa_SiteJournalDeals { get; set; }

        public bool pa_TransCopyAtReceiving { get; set; }

        public bool pa_TransCopyAtGrouping { get; set; }

        public bool pa_TransCopyIfDataAmbiguous { get; set; }

        public bool pa_TransRegisterPayment { get; set; }

        public bool pa_AutoSaleReceive { get; set; }

        public int pa_AutoSaleReceiveInterval { get; set; }

        public bool pa_AutoSaleReceiveFull { get; set; }

        public int pa_AutoSaleReceiveFullPeriod { get; set; }

        public int pa_AutoAuctionSendOption { get; set; }

        public int pa_AutoAuctionSendInterval { get; set; }

        public bool pa_AutoAuctionLastingReceive { get; set; }

        public int pa_AutoAuctionLastingReceiveInterval { get; set; }

        public bool pa_AutoAuctionEndedReceive { get; set; }

        public int pa_AutoAuctionEndedReceiveInterval { get; set; }

        public int pa_AutoAuctionReceiveFullData { get; set; }

        public int pa_AutoAuctionReceiveFullDataPeriod { get; set; }

        public int pa_AutoEmailSendOption { get; set; }

        public int pa_AutoEmailSendInterval { get; set; }

        public bool pa_AutoEmailReceive { get; set; }

        public int pa_AutoEmailReceiveInterval { get; set; }

        public int pa_AutoFeedbackSendOption { get; set; }

        public int pa_AutoFeedbackSendInterval { get; set; }

        public int pa_AutoFeedbackReceiveOption { get; set; }

        public int pa_AutoFeedbackReceiveLastCount { get; set; }

        public int pa_AutoFeedbackReceiveNewestDaysCount { get; set; }

        public int pa_AutoFeedbackReceiveInterval { get; set; }

        public bool pa_AutoFeedbackReceiveBuyersOnly { get; set; }

        public bool pa_AutoEPSend { get; set; }

        public int pa_AutoEPSendInterval { get; set; }

        public bool pa_AutoEPItemsReceive { get; set; }

        public int pa_AutoEPItemsReceiveInterval { get; set; }

        public bool pa_AutoEPImportOrders { get; set; }

        public int pa_AutoEPImportOrdersInterval { get; set; }

        public bool pa_AutoDelivererPackageCreate { get; set; }

        public int pa_AutoDelivererPackageCreateInterval { get; set; }

        public bool pa_AutoDelivererPackagePayment { get; set; }

        public int pa_AutoDelivererPackagePaymentInterval { get; set; }

        public bool pa_AutoDelivererStatusReceive { get; set; }

        public int pa_AutoDelivererStatusReceiveInterval { get; set; }

        public int pa_AutoAuctionSendPendingChangesOption { get; set; }

        public int pa_AutoAuctionSendPendingChangesInterval { get; set; }

        public int pa_DefaultCategoryForSyncParams { get; set; }

        public bool pa_DefaultPackageInfoSend { get; set; }

        public bool pa_AutoPackageNumberToAllegroSend { get; set; }

        public int pa_AutoPackageNumberToAllegroSendInterval { get; set; }

        public virtual sl_User sl_User { get; set; }
    }
}
