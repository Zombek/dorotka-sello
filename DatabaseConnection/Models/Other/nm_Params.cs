namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class nm_Params
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nm_ParamId { get; set; }

        public DateTime? nm_NextRSSDownload { get; set; }

        public int? nm_RSSDownloadInterval { get; set; }

        public DateTime? nm_NextLicenseCheck { get; set; }

        public int? nm_LicenseCheckInterval { get; set; }

        public DateTime? nm_NextConnectionLoggingCheck { get; set; }

        public int? nm_ConnectionLoggingCheckInterval { get; set; }

        public DateTime? nm_NextSchedulerIsOffCheck { get; set; }

        public int? nm_SchedulerIsOffCheckInterval { get; set; }

        public DateTime? nm_NextEmailLoggingCheck { get; set; }

        public int? nm_EmailLoggingCheckInterval { get; set; }

        public DateTime? nm_NextOldLogEntriesRemoveCheck { get; set; }

        public int? nm_OldLogEntriesRemoveCheckInterval { get; set; }

        public DateTime? nm_NextOverdueTransactionsCheck { get; set; }

        public int? nm_OverdueTransactionsCheckInterval { get; set; }

        public int? nm_BadgeUpdateInterval { get; set; }

        public DateTime? nm_NextGarbageCollectorRun { get; set; }

        public int? nm_GarbageCollectorInterval { get; set; }

        public DateTime? nm_NextDataBaseCheck { get; set; }

        public int? nm_DataBaseCheckInterval { get; set; }
    }
}
