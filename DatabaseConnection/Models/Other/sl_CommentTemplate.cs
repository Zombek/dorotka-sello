namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_CommentTemplate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ct_Id { get; set; }

        public int ct_Score { get; set; }

        [StringLength(25)]
        public string ct_Name { get; set; }

        [Required]
        [StringLength(255)]
        public string ct_Text { get; set; }

        public bool ct_Automatic { get; set; }
    }
}
