namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_CustPhone
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cp_Id { get; set; }

        public int cp_CustomerId { get; set; }

        [Required]
        [StringLength(50)]
        public string cp_Phone { get; set; }

        public bool cp_Default { get; set; }

        public virtual cs__Customer cs__Customer { get; set; }
    }
}
