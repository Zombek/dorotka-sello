namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class jb_Message
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int jm_Id { get; set; }

        public int jm_JobId { get; set; }

        public DateTime jm_Time { get; set; }

        [Required]
        [StringLength(2000)]
        public string jm_Text { get; set; }

        public int jm_Status { get; set; }
    }
}
