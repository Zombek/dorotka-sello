namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class gt_Atrybut
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int gta_Id { get; set; }

        public int gta_IdObiektu { get; set; }

        public int gta_IdObiektuGt { get; set; }

        [Required]
        [StringLength(50)]
        public string gta_Nazwa { get; set; }

        [Required]
        [StringLength(1000)]
        public string gta_Wartosc { get; set; }
    }
}
