namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em_Source
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int es_Id { get; set; }

        public int es_EmailId { get; set; }

        [Column(TypeName = "text")]
        public string es_Source { get; set; }

        public virtual em__Email em__Email { get; set; }
    }
}
