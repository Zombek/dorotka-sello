namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_Picture
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ap_Id { get; set; }

        public int ap_AuctionId { get; set; }

        public int? ap_PictureId { get; set; }

        public int? ap_TagId { get; set; }

        public int ap_PictureOrder { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual im__Image im__Image { get; set; }
    }
}
