namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class gt_Definicja
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public gt_Definicja()
        {
            gt__Obiekt = new HashSet<gt__Obiekt>();
            gt_TransObiekt = new HashSet<gt_TransObiekt>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int gtd_Id { get; set; }

        public int gtd_ObiektId { get; set; }

        [Required]
        [StringLength(50)]
        public string gtd_Nazwa { get; set; }

        [Column(TypeName = "image")]
        public byte[] gtd_Definicja { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<gt__Obiekt> gt__Obiekt { get; set; }

        public virtual gt__Obiekt gt__Obiekt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<gt_TransObiekt> gt_TransObiekt { get; set; }
    }
}
