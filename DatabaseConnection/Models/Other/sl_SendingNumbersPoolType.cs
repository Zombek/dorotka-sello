namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_SendingNumbersPoolType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_SendingNumbersPoolType()
        {
            sl_Delivery = new HashSet<sl_Delivery>();
            sl_SendingNumbersPool = new HashSet<sl_SendingNumbersPool>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int npt_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string npt_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sl_Delivery> sl_Delivery { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sl_SendingNumbersPool> sl_SendingNumbersPool { get; set; }
    }
}
