namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_CurrencyExchangeRates
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_CurrencyExchangeRates()
        {
            sl_CurrencyExchangeRatesEntry = new HashSet<sl_CurrencyExchangeRatesEntry>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int er_ID { get; set; }

        public DateTime er_Data { get; set; }

        public int er_BankId { get; set; }

        [Required]
        [StringLength(50)]
        public string er_Name { get; set; }

        public virtual sl_CurrencyBank sl_CurrencyBank { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sl_CurrencyExchangeRatesEntry> sl_CurrencyExchangeRatesEntry { get; set; }
    }
}
