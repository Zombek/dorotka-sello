namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pf_PrintForm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pf_PrintForm()
        {
            pf_Delivery = new HashSet<pf_Delivery>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pf_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string pf_Description { get; set; }

        [Column(TypeName = "image")]
        public byte[] pf_Form { get; set; }

        public bool pf_Ready { get; set; }

        public bool pf_Common { get; set; }

        public int pf_Type { get; set; }

        [StringLength(100)]
        public string pf_PrinterName { get; set; }

        public int pf_NumberOfCopies { get; set; }

        public bool pf_SelectedDefault { get; set; }

        public int? pf_ExternalDeliverer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pf_Delivery> pf_Delivery { get; set; }
    }
}
