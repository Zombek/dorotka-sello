namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class xcs_Szum
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int szcs_Id { get; set; }

        public int szcs_ParamId { get; set; }

        [Required]
        [StringLength(50)]
        public string szcs_Item { get; set; }

        public virtual insx_Parametr insx_Parametr { get; set; }
    }
}
