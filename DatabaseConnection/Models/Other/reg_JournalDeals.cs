namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class reg_JournalDeals
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int rjd_Id { get; set; }

        public int rjd_RegId { get; set; }

        public int rjd_SiteId { get; set; }

        public long rjd_DealEventId { get; set; }

        public int rjd_DealEventType { get; set; }

        public DateTime rjd_DealEventTime { get; set; }

        public long rjd_DealId { get; set; }

        public long? rjd_DealTransactionId { get; set; }

        public long rjd_DealItemId { get; set; }

        public int rjd_DealBuyerId { get; set; }

        public int rjd_DealQuantity { get; set; }

        public bool rjd_Processed { get; set; }

        public virtual reg__Registration reg__Registration { get; set; }

        public virtual st__Site st__Site { get; set; }
    }
}
