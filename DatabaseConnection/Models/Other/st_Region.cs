namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_Region
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_Region()
        {
            au__Auction = new HashSet<au__Auction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sr_Id { get; set; }

        public int sr_SvcProvId { get; set; }

        public int sr_SiteId { get; set; }

        [Required]
        [StringLength(255)]
        public string sr_Name { get; set; }

        public int? sr_Ident { get; set; }

        [StringLength(50)]
        public string sr_Code { get; set; }

        public bool? sr_Expired { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }
    }
}
