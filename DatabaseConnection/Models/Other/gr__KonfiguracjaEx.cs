namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class gr__KonfiguracjaEx
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string cge_Section { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cge_User { get; set; }

        [StringLength(50)]
        public string cge_Version { get; set; }

        [Column(TypeName = "image")]
        public byte[] cge_BinListInfo { get; set; }

        [Column(TypeName = "image")]
        public byte[] cge_BinColumns { get; set; }

        [Column(TypeName = "image")]
        public byte[] cge_BinTables { get; set; }

        [Column(TypeName = "image")]
        public byte[] cge_BinVisibility { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cge_Service { get; set; }

        [Column(TypeName = "text")]
        public string cge_Sql { get; set; }

        [StringLength(50)]
        public string cge_LastUpdateVersion { get; set; }
    }
}
