namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ep_RegData
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int erd_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int erd_RegId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string erd_Name { get; set; }

        [StringLength(3500)]
        public string erd_vvarchar { get; set; }

        public int? erd_vint { get; set; }

        [Column(TypeName = "money")]
        public decimal? erd_vmoney { get; set; }

        public DateTime? erd_vdatetime { get; set; }

        [MaxLength(3500)]
        public byte[] erd_vbinary { get; set; }

        public bool? erd_vbit { get; set; }

        public virtual ep__Registration ep__Registration { get; set; }
    }
}
