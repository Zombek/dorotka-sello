namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd__Podmiot
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pd__Podmiot()
        {
            pd_BankAccounts = new HashSet<pd_BankAccounts>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pd_Id { get; set; }

        public bool pd_Connected { get; set; }

        [Required]
        [StringLength(3)]
        public string pd_BaseCurrency { get; set; }

        public int pd_ExchangeRateType { get; set; }

        [Required]
        [StringLength(50)]
        public string pd_Location { get; set; }

        public int pd_IR_ThumbnailCX { get; set; }

        public int pd_IR_ThumbnailCY { get; set; }

        [Required]
        [StringLength(64)]
        public string pd_Name { get; set; }

        [Required]
        [StringLength(64)]
        public string pd_Surname { get; set; }

        public bool pd_Company { get; set; }

        [Required]
        [StringLength(250)]
        public string pd_CompanyName { get; set; }

        [Required]
        [StringLength(20)]
        public string pd_NIP { get; set; }

        [Required]
        [StringLength(20)]
        public string pd_REGON { get; set; }

        [Required]
        [StringLength(255)]
        public string pd_Address { get; set; }

        [Required]
        [StringLength(64)]
        public string pd_Community { get; set; }

        [Required]
        [StringLength(32)]
        public string pd_PostCode { get; set; }

        [Required]
        [StringLength(64)]
        public string pd_City { get; set; }

        [Required]
        [StringLength(64)]
        public string pd_Region { get; set; }

        [Required]
        [StringLength(50)]
        public string pd_Country { get; set; }

        [Required]
        [StringLength(255)]
        public string pd_wwwPage { get; set; }

        [Required]
        [StringLength(255)]
        public string pd_emailAddress { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string pd_HtmlSignature { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string pd_HtmlRules { get; set; }

        [Column(TypeName = "image")]
        public byte[] pd_Lirm { get; set; }

        [StringLength(50)]
        public string pd_Phone { get; set; }

        public Guid? pd_GUID { get; set; }

        [Required]
        [StringLength(50)]
        public string pd_GUID2 { get; set; }

        public bool? pd_TelemetriaZgoda { get; set; }

        public DateTime? pd_TelemetriaZgodaPytano { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_BankAccounts> pd_BankAccounts { get; set; }
    }
}
