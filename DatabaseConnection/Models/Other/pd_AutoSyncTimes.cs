namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd_AutoSyncTimes
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pp_Id { get; set; }

        public int? pp_LastAuctionLastingReceive { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pp_AuctionLastingReceiveCounter { get; set; }

        public int? pp_LastAuctionEndedReceive { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pp_AuctionEndedReceiveCounter { get; set; }

        public int? pp_LastAuctionSend { get; set; }

        public int? pp_LastSaleReceive { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pp_SaleReceiveCounter { get; set; }

        public int? pp_LastFeedbackReceive { get; set; }

        public int? pp_LastFeedbackSend { get; set; }

        public int? pp_LastEmailReceive { get; set; }

        public int? pp_LastEmailSend { get; set; }

        public int? pp_LastEPSend { get; set; }

        public int? pp_LastEPItemsReceive { get; set; }

        public int? pp_LastEPImportOrders { get; set; }

        public int? pp_LastDelivererPackageCreate { get; set; }

        public int? pp_LastDelivererPackagePayment { get; set; }

        public int? pp_LastDelivererStatusReceive { get; set; }

        public int? pp_LastAuctionSendPendingChanges { get; set; }

        public int? pp_LastPackageNumberToAllegroSend { get; set; }
    }
}
