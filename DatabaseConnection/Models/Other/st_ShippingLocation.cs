namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_ShippingLocation
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ssl_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ssl_SvcProvId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ssl_SiteId { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(255)]
        public string ssl_Name { get; set; }

        public int? ssl_Ident { get; set; }

        [StringLength(50)]
        public string ssl_Code { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool ssl_Expired { get; set; }
    }
}
