namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_Language
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_Language()
        {
            tp__HtmlTemplate = new HashSet<tp__HtmlTemplate>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ln_Id { get; set; }

        [Required]
        [StringLength(3)]
        public string ln_Symbol { get; set; }

        [Required]
        [StringLength(50)]
        public string ln_Name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tp__HtmlTemplate> tp__HtmlTemplate { get; set; }
    }
}
