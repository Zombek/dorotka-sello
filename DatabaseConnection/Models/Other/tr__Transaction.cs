namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tr__Transaction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tr__Transaction()
        {
            adr__Address = new HashSet<adr__Address>();
            em__Email = new HashSet<em__Email>();
            em__Email1 = new HashSet<em__Email>();
            pb_PostBuyFormTransaction = new HashSet<pb_PostBuyFormTransaction>();
            pc__Package = new HashSet<pc__Package>();
            tr_Comment = new HashSet<tr_Comment>();
            tr_Group1 = new HashSet<tr_Group>();
            tr_Group2 = new HashSet<tr_Group>();
            tr_Item = new HashSet<tr_Item>();
            tr_Property = new HashSet<tr_Property>();
            tr_Item1 = new HashSet<tr_Item>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tr_Id { get; set; }

        public bool tr_Grouped { get; set; }

        public bool tr_Group { get; set; }

        public DateTime tr_CreationDate { get; set; }

        [Required]
        [StringLength(100)]
        public string tr_Source { get; set; }

        public bool tr_Paid { get; set; }

        public int tr_CustomerId { get; set; }

        public int? tr_DelivererId { get; set; }

        public int? tr_DeliveryId { get; set; }

        public bool tr_PayOnDelivery { get; set; }

        [Column(TypeName = "money")]
        public decimal tr_DeliveryCost { get; set; }

        [Column(TypeName = "money")]
        public decimal tr_Payment { get; set; }

        [Column(TypeName = "money")]
        public decimal? tr_Remittance { get; set; }

        public DateTime? tr_RemittanceDate { get; set; }

        public int tr_PackageCreationMethod { get; set; }

        public int tr_Complete { get; set; }

        public bool tr_ExNihilo { get; set; }

        public int? tr_AuctionId { get; set; }

        public int tr_RegId { get; set; }

        public int tr_SiteId { get; set; }

        public int tr_OwnerId { get; set; }

        public DateTime? tr_LastModified { get; set; }

        public int tr_DiscountType { get; set; }

        [Column(TypeName = "money")]
        public decimal tr_DiscountPercent { get; set; }

        [Column(TypeName = "money")]
        public decimal tr_DiscountAmount { get; set; }

        [Required]
        [StringLength(3)]
        public string tr_BaseCurrency { get; set; }

        [Required]
        [StringLength(3)]
        public string tr_PaymentCurrency { get; set; }

        [Column(TypeName = "money")]
        public decimal tr_ExchangeRate { get; set; }

        public bool tr_PackingComplete { get; set; }

        public bool tr_DeliveryComplete { get; set; }

        public bool tr_InvolveComments { get; set; }

        public int tr_OrigQuantity { get; set; }

        public int? tr_InitDocType { get; set; }

        [Required]
        [StringLength(50)]
        public string tr_InitDocDescr { get; set; }

        public int? tr_InitDocId { get; set; }

        public int? tr_DoneDocType { get; set; }

        [Required]
        [StringLength(50)]
        public string tr_DoneDocDescr { get; set; }

        public int? tr_DoneDocId { get; set; }

        public int? tr_InitDocRegId { get; set; }

        public int? tr_DoneDocRegId { get; set; }

        public bool tr_InitDocDirty { get; set; }

        public bool tr_DoneDocDirty { get; set; }

        public int tr_DoneDocCreationMethod { get; set; }

        public bool tr_AutoPacking { get; set; }

        [StringLength(50)]
        public string tr_Number { get; set; }

        public bool tr_AutoCommentDisabled { get; set; }

        public bool tr_AutoMailDisabled { get; set; }

        public bool tr_CommentDisabled { get; set; }

        public int tr_BuyerFormStatus { get; set; }

        public int tr_AutoPaymentStatus { get; set; }

        public bool tr_IgnoreDifferencesWithBuyerForm { get; set; }

        public bool tr_DoNotAutoCopyDataFromForms { get; set; }

        public bool tr_IsOverdueMailSent { get; set; }

        public bool tr_EvaluateWeightFromItems { get; set; }

        [Column(TypeName = "money")]
        public decimal tr_Weight { get; set; }

        public long? tr_ExternalDealId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adr__Address> adr__Address { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual cs__Customer cs__Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em__Email> em__Email { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em__Email> em__Email1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pb_PostBuyFormTransaction> pb_PostBuyFormTransaction { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package { get; set; }

        public virtual reg__Registration reg__Registration { get; set; }

        public virtual reg_Site reg_Site { get; set; }

        public virtual sl_Deliverer sl_Deliverer { get; set; }

        public virtual sl_Delivery sl_Delivery { get; set; }

        public virtual sl_Delivery sl_Delivery1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Comment> tr_Comment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Group> tr_Group1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Group> tr_Group2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Item> tr_Item { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Property> tr_Property { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Item> tr_Item1 { get; set; }
    }
}
