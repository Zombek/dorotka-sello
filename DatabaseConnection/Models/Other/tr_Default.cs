namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tr_Default
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int td_Id { get; set; }

        public int? td_DelivererId { get; set; }

        public int? td_DelivererIdPOD { get; set; }

        public int? td_DeliveryId { get; set; }

        public int? td_DeliveryIdPOD { get; set; }

        public int? td_PackageCreationMethod { get; set; }

        public int td_DoneDocCreationMethod { get; set; }

        public bool td_AutoPackingPOD { get; set; }

        public bool td_AutoPackingPODChangeOnly { get; set; }

        public bool td_AutoPackingAskPOD { get; set; }

        public bool td_AutoPacking { get; set; }

        public bool td_AutoPackingChangeOnly { get; set; }

        public bool td_AutoPackingAsk { get; set; }

        public int td_DefaultCost { get; set; }

        [Column(TypeName = "money")]
        public decimal td_OwnDefaultCost { get; set; }

        public int? td_OverdueDays { get; set; }

        public int td_PackageUpdateMethod { get; set; }

        [Required]
        [StringLength(255)]
        public string td_UserAddedTransactionSourceTemplate { get; set; }

        [Required]
        [StringLength(255)]
        public string td_GroupTransactionSourceTemplate { get; set; }

        public int? td_ManualTransactionProperty1 { get; set; }

        public int? td_ManualTransactionProperty2 { get; set; }

        public int? td_ManualTransactionProperty3 { get; set; }

        public virtual sl_Deliverer sl_Deliverer { get; set; }

        public virtual sl_Deliverer sl_Deliverer1 { get; set; }

        public virtual sl_Delivery sl_Delivery { get; set; }

        public virtual sl_Delivery sl_Delivery1 { get; set; }

        public virtual sl_TransProperty sl_TransProperty { get; set; }

        public virtual sl_TransProperty sl_TransProperty1 { get; set; }

        public virtual sl_TransProperty sl_TransProperty2 { get; set; }
    }
}
