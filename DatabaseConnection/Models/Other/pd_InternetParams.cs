namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd_InternetParams
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pi_Id { get; set; }

        public bool pi_ProxyAuto { get; set; }

        [Required]
        [StringLength(50)]
        public string pi_ProxyAddress { get; set; }

        public int pi_ProxyPort { get; set; }

        [Required]
        [StringLength(50)]
        public string pi_ProxyUser { get; set; }

        [Required]
        [StringLength(50)]
        public string pi_ProxyPass { get; set; }
    }
}
