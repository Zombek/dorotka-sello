namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_ExportType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ex_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string ex_Name { get; set; }

        public int ex_DataToExport { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string ex_Xslt { get; set; }

        [Required]
        [StringLength(1024)]
        public string ex_OutputFile { get; set; }

        public bool ex_Default { get; set; }

        public bool ex_Active { get; set; }
    }
}
