namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ins_ident
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ido_wartosc { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string ido_nazwa { get; set; }

        public int? ido_minIdValue { get; set; }
    }
}
