namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au__Auction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public au__Auction()
        {
            au_Picture = new HashSet<au_Picture>();
            au_CategorySpecific = new HashSet<au_CategorySpecific>();
            au_Allegro = new HashSet<au_Allegro>();
            au_DescriptionImage = new HashSet<au_DescriptionImage>();
            au_Ebay = new HashSet<au_Ebay>();
            au_Enhancement = new HashSet<au_Enhancement>();
            au_Features = new HashSet<au_Features>();
            au_Note1 = new HashSet<au_Note>();
            au_Payment = new HashSet<au_Payment>();
            au_PendingChanges = new HashSet<au_PendingChanges>();
            au_Shipping = new HashSet<au_Shipping>();
            au_TransactionProperty = new HashSet<au_TransactionProperty>();
            em__Email = new HashSet<em__Email>();
            it_AuctionTemplate = new HashSet<it_AuctionTemplate>();
            tr__Transaction = new HashSet<tr__Transaction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int au_Id { get; set; }

        public int? au_RegId { get; set; }

        public int? au_RegSiteId { get; set; }

        public int au_SvcProvId { get; set; }

        public int? au_SiteId { get; set; }

        public int? au_UserId { get; set; }

        public int? au_GroupId { get; set; }

        public int au_State { get; set; }

        [Required]
        [StringLength(255)]
        public string au_Title { get; set; }

        [StringLength(50)]
        public string au_Number { get; set; }

        public int? au_CatId { get; set; }

        [Required]
        [StringLength(255)]
        public string au_CatName { get; set; }

        public bool au_Series { get; set; }

        public int? au_TemplateId { get; set; }

        public int? au_ProductId { get; set; }

        public int au_Quantity { get; set; }

        public int au_TypeId { get; set; }

        [Column(TypeName = "money")]
        public decimal? au_StartPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal? au_MinPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal? au_BINPrice { get; set; }

        [Required]
        [StringLength(3)]
        public string au_Currency { get; set; }

        public DateTime? au_StartDate { get; set; }

        public int? au_DurationId { get; set; }

        public DateTime? au_EndDate { get; set; }

        public int au_HitCount { get; set; }

        [Column(TypeName = "money")]
        public decimal au_HighestBid { get; set; }

        public int au_BidCount { get; set; }

        public int au_Sold { get; set; }

        [Column(TypeName = "money")]
        public decimal au_Fee { get; set; }

        [Column(TypeName = "money")]
        public decimal au_Commission { get; set; }

        [Required]
        [StringLength(1000)]
        public string au_Note { get; set; }

        public int? au_CountryId { get; set; }

        public int? au_RegionId { get; set; }

        [Required]
        [StringLength(32)]
        public string au_PostalCode { get; set; }

        [Required]
        [StringLength(100)]
        public string au_Location { get; set; }

        public int au_Type { get; set; }

        public int au_WatcherCount { get; set; }

        public int? au_HtmlTemplateId { get; set; }

        public int? au_TemplGroupId { get; set; }

        public int? au_ShopCatId { get; set; }

        public int? au_ShopAutolisting { get; set; }

        [StringLength(255)]
        public string au_ShopCatName { get; set; }

        public bool au_Private { get; set; }

        public bool au_ShopListing { get; set; }

        [StringLength(10)]
        public string au_EnhancementOptionsAbbr { get; set; }

        public int au_StartAuctionScheduler { get; set; }

        public int? au_PricingListId { get; set; }

        public int au_AutoQuantitySyncMethod { get; set; }

        public int au_AutoFinishMethod { get; set; }

        public int au_AutoFinishQuantity { get; set; }

        public int? au_ThumbnailId { get; set; }

        [Required]
        [StringLength(100)]
        public string au_EAN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Picture> au_Picture { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_CategorySpecific> au_CategorySpecific { get; set; }

        public virtual im__Image im__Image { get; set; }

        public virtual it__Item it__Item { get; set; }

        public virtual reg__Registration reg__Registration { get; set; }

        public virtual sl_AuctionGroup sl_AuctionGroup { get; set; }

        public virtual sl_AuctionTemplateGroup sl_AuctionTemplateGroup { get; set; }

        public virtual sl_User sl_User { get; set; }

        public virtual st__Site st__Site { get; set; }

        public virtual st_AuctionType st_AuctionType { get; set; }

        public virtual st_Category st_Category { get; set; }

        public virtual st_Country st_Country { get; set; }

        public virtual st_Duration st_Duration { get; set; }

        public virtual st_Region st_Region { get; set; }

        public virtual tp__HtmlTemplate tp__HtmlTemplate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Allegro> au_Allegro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_DescriptionImage> au_DescriptionImage { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Ebay> au_Ebay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Enhancement> au_Enhancement { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Features> au_Features { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Note> au_Note1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Payment> au_Payment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_PendingChanges> au_PendingChanges { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Shipping> au_Shipping { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_TransactionProperty> au_TransactionProperty { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em__Email> em__Email { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_AuctionTemplate> it_AuctionTemplate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr__Transaction> tr__Transaction { get; set; }
    }
}
