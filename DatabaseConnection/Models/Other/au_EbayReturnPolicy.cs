namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_EbayReturnPolicy
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int aurp_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int aurp_EbayAuctionId { get; set; }

        [Column(TypeName = "ntext")]
        public string aurp_Description { get; set; }

        [StringLength(15)]
        public string aurp_EAN { get; set; }

        public int? aurp_Refund { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool aurp_ReturnsAccepted { get; set; }

        public int? aurp_ReturnsWithin { get; set; }

        public int? aurp_ShippingCostPaidBy { get; set; }

        public int? aurp_WarrantyDuration { get; set; }

        public int? aurp_WarrantyType { get; set; }
    }
}
