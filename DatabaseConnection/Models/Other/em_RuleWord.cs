namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em_RuleWord
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ew_Id { get; set; }

        public int ew_RuleId { get; set; }

        public int ew_Field { get; set; }

        [Required]
        [StringLength(100)]
        public string ew_Word { get; set; }

        public virtual em_Rule em_Rule { get; set; }
    }
}
