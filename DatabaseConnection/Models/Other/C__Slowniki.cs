namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("__Slowniki")]
    public partial class C__Slowniki
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Tabela { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(100)]
        public string SelectTwardosci { get; set; }

        [StringLength(50)]
        public string Id_Tabeli { get; set; }
    }
}
