namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_PackageSynchronizationParams
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sp_Id { get; set; }

        public int sp_DeliveryId { get; set; }

        [Required]
        [StringLength(50)]
        public string sp_Name { get; set; }

        [StringLength(3500)]
        public string sp_vvarchar { get; set; }

        public int? sp_vint { get; set; }

        [Column(TypeName = "money")]
        public decimal? sp_vmoney { get; set; }

        public DateTime? sp_vdatetime { get; set; }

        [MaxLength(3500)]
        public byte[] sp_vbinary { get; set; }

        public bool? sp_vbit { get; set; }
    }
}
