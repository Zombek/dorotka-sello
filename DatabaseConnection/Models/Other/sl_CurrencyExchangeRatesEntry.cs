namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_CurrencyExchangeRatesEntry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ere_Id { get; set; }

        public int ere_ExchangeRatesId { get; set; }

        [Required]
        [StringLength(3)]
        public string ere_CurrencyId { get; set; }

        [Column(TypeName = "money")]
        public decimal ere_Purchase { get; set; }

        [Column(TypeName = "money")]
        public decimal ere_Sell { get; set; }

        [Column(TypeName = "money")]
        public decimal ere_Avg { get; set; }

        public virtual sl_CurrencyExchangeRates sl_CurrencyExchangeRates { get; set; }
    }
}
