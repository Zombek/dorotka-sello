namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class gt_Transformacja
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public gt_Transformacja()
        {
            gt__Obiekt = new HashSet<gt__Obiekt>();
            gt_TransObiekt = new HashSet<gt_TransObiekt>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int gtt_Id { get; set; }

        public int gtt_ObiektId { get; set; }

        [Required]
        [StringLength(50)]
        public string gtt_Nazwa { get; set; }

        public int gtt_RodzajId { get; set; }

        [Column(TypeName = "image")]
        public byte[] gtt_Transformacja { get; set; }

        [StringLength(255)]
        public string gtt_Schemat { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<gt__Obiekt> gt__Obiekt { get; set; }

        public virtual gt__Obiekt gt__Obiekt1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<gt_TransObiekt> gt_TransObiekt { get; set; }
    }
}
