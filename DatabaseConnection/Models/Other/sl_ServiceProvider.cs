namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_ServiceProvider
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_ServiceProvider()
        {
            cs__Customer = new HashSet<cs__Customer>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ss_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string ss_Name { get; set; }

        [Required]
        [StringLength(255)]
        public string ss_ProgId { get; set; }

        [StringLength(255)]
        public string ss_AuctionProgId { get; set; }

        [StringLength(255)]
        public string ss_TemplateProgId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs__Customer> cs__Customer { get; set; }
    }
}
