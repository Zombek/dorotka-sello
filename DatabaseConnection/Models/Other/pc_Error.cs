namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pc_Error
    {
        [Key]
        public int pe_Id { get; set; }

        public int pe_ParentId { get; set; }

        public int pe_Code { get; set; }

        [Required]
        [StringLength(255)]
        public string pe_Description { get; set; }

        public int pe_Source { get; set; }

        public virtual pc__Package pc__Package { get; set; }
    }
}
