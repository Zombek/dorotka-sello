namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em_Template
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public em_Template()
        {
            tr_AutoMsgParams = new HashSet<tr_AutoMsgParams>();
            tr_AutoMsgParams1 = new HashSet<tr_AutoMsgParams>();
            tr_AutoMsgParams2 = new HashSet<tr_AutoMsgParams>();
            tr_AutoMsgParams3 = new HashSet<tr_AutoMsgParams>();
            tr_AutoMsgParams4 = new HashSet<tr_AutoMsgParams>();
            tr_AutoMsgParams5 = new HashSet<tr_AutoMsgParams>();
            tr_AutoMsgParams6 = new HashSet<tr_AutoMsgParams>();
            tr_AutoMsgParams7 = new HashSet<tr_AutoMsgParams>();
            tr_AutoMsgParams8 = new HashSet<tr_AutoMsgParams>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int et_Id { get; set; }

        [Required]
        [StringLength(255)]
        public string et_Name { get; set; }

        [Required]
        [StringLength(1000)]
        public string et_Subject { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string et_Text { get; set; }

        public bool et_HTML { get; set; }

        [Required]
        [StringLength(1024)]
        public string et_AttachmentsString { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams3 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams4 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams5 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams6 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams7 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_AutoMsgParams> tr_AutoMsgParams8 { get; set; }
    }
}
