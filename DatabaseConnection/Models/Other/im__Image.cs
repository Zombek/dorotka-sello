namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class im__Image
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public im__Image()
        {
            au__Auction = new HashSet<au__Auction>();
            au_DescriptionImage = new HashSet<au_DescriptionImage>();
            au_Picture = new HashSet<au_Picture>();
            it_ItemPictures = new HashSet<it_ItemPictures>();
            tp_Image = new HashSet<tp_Image>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int im_Id { get; set; }

        [Required]
        [StringLength(260)]
        public string im_Name { get; set; }

        [Column(TypeName = "image")]
        public byte[] im_Image { get; set; }

        [Column(TypeName = "image")]
        public byte[] im_Preview { get; set; }

        [Required]
        [StringLength(512)]
        public string im_SrcPath { get; set; }

        [StringLength(32)]
        public string im_CheckSum { get; set; }

        [StringLength(700)]
        public string im_FtpAddress { get; set; }

        [StringLength(700)]
        public string im_HttpAddress { get; set; }

        public DateTime? im_FtpAddressLastMod { get; set; }

        public int? im_Width { get; set; }

        public int? im_Height { get; set; }

        public int? im_Size { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_DescriptionImage> au_DescriptionImage { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Picture> au_Picture { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_ItemPictures> it_ItemPictures { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tp_Image> tp_Image { get; set; }
    }
}
