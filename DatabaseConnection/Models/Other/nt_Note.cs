namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class nt_Note
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ne_id { get; set; }

        public int? ne_ObjectId { get; set; }

        public int ne_ObjectType { get; set; }

        public int ne_Order { get; set; }

        public DateTime ne_CreationDate { get; set; }

        public int? ne_UserId { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string ne_Content { get; set; }

        public bool ne_ToPrint { get; set; }

        public bool ne_ToStatuses { get; set; }

        public bool ne_ToDocument { get; set; }

        public bool ne_Important { get; set; }

        public bool ne_Done { get; set; }

        public bool ne_DefaultTemplate { get; set; }

        public virtual sl_User sl_User { get; set; }
    }
}
