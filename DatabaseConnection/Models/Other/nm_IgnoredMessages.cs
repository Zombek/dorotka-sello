namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class nm_IgnoredMessages
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nm_IgnoredId { get; set; }

        public int nm_UserId { get; set; }

        public int nm_MessageId { get; set; }

        public virtual nm_NotifierMessage nm_NotifierMessage { get; set; }
    }
}
