namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tr_Group
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tg_Id { get; set; }

        public int tg_TransId { get; set; }

        public int tg_PrimaryTransId { get; set; }

        public virtual tr__Transaction tr__Transaction { get; set; }

        public virtual tr__Transaction tr__Transaction1 { get; set; }
    }
}
