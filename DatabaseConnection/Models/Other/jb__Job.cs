namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class jb__Job
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int lg_Id { get; set; }

        public int lg_UserId { get; set; }

        public int lg_Type { get; set; }

        public int lg_Subtype { get; set; }

        public DateTime lg_Begin { get; set; }

        public DateTime? lg_End { get; set; }

        public int? lg_AffectedCount { get; set; }

        public int? lg_Result { get; set; }

        public virtual sl_User sl_User { get; set; }
    }
}
