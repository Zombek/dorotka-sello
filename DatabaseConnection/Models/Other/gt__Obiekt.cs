namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class gt__Obiekt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public gt__Obiekt()
        {
            gt_Definicja1 = new HashSet<gt_Definicja>();
            gt_Transformacja1 = new HashSet<gt_Transformacja>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int gto_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string gto_Nazwa { get; set; }

        public int? gto_PodstDefId { get; set; }

        public int? gto_PodstTransId { get; set; }

        public virtual gt_Definicja gt_Definicja { get; set; }

        public virtual gt_Transformacja gt_Transformacja { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<gt_Definicja> gt_Definicja1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<gt_Transformacja> gt_Transformacja1 { get; set; }
    }
}
