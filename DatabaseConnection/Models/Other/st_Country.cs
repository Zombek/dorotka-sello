namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_Country
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_Country()
        {
            au__Auction = new HashSet<au__Auction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sc_Id { get; set; }

        public int sc_SvcProvId { get; set; }

        public int sc_SiteId { get; set; }

        [Required]
        [StringLength(255)]
        public string sc_Name { get; set; }

        public int? sc_Ident { get; set; }

        [StringLength(64)]
        public string sc_Code { get; set; }

        public bool? sc_Expired { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }
    }
}
