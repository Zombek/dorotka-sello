namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class it_ExternalCustomAttributes
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int eca_Id { get; set; }

        public int eca_ItemId { get; set; }

        public int eca_AttributeId { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string eca_Value { get; set; }

        public virtual it__Item it__Item { get; set; }

        public virtual it_ExternalCustomAttributesDict it_ExternalCustomAttributesDict { get; set; }
    }
}
