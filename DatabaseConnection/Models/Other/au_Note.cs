namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_Note
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int an_Id { get; set; }

        public int an_AuctionId { get; set; }

        public DateTime? an_Date { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string an_Description { get; set; }

        public virtual au__Auction au__Auction { get; set; }
    }
}
