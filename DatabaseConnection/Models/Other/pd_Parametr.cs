namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd_Parametr
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pdp_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool pdp_ArchPytaj { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool pdp_ArchNaZakoncz { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool pdp_ArchOkresowa { get; set; }

        public int? pdp_ArchInterwal { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool pdp_ArchNaWybrKomp { get; set; }

        [StringLength(50)]
        public string pdp_ArchKomputer { get; set; }

        [Key]
        [Column(Order = 5)]
        public bool pdp_ArchPokazUst { get; set; }

        public DateTime? pdp_ArchDataOst { get; set; }

        [StringLength(51)]
        public string pdp_ArchUzytkOst { get; set; }
    }
}
