namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class nm_NotifierMessage
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public nm_NotifierMessage()
        {
            nm_IgnoredMessages = new HashSet<nm_IgnoredMessages>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int nm_Id { get; set; }

        public int nm_MessageId { get; set; }

        [Required]
        [StringLength(1024)]
        public string nm_Title { get; set; }

        [Column(TypeName = "ntext")]
        public string nm_Text { get; set; }

        public int nm_CategoryId { get; set; }

        public int nm_SubcategoryId { get; set; }

        public DateTime? nm_NotificationTime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<nm_IgnoredMessages> nm_IgnoredMessages { get; set; }
    }
}
