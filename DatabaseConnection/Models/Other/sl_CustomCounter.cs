namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_CustomCounter
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cc_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string cc_Name { get; set; }

        public int cc_NextValue { get; set; }
    }
}
