namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd_UserPermission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int up_Id { get; set; }

        public int up_RightId { get; set; }

        public int up_UserId { get; set; }

        public virtual sl_User sl_User { get; set; }
    }
}
