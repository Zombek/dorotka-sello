namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_CategorySpecificDict
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int csd_Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int csd_CatSpecId { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int csd_Ident { get; set; }

        [StringLength(50)]
        public string csd_Code { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(255)]
        public string csd_Name { get; set; }

        public int? csd_Position { get; set; }

        public virtual st_CategorySpecific st_CategorySpecific { get; set; }
    }
}
