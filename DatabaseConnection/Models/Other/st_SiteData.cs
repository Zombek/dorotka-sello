namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_SiteData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sd_Id { get; set; }

        public int sd_SiteId { get; set; }

        [Required]
        [StringLength(50)]
        public string sd_Name { get; set; }

        [StringLength(3500)]
        public string sd_vvarchar { get; set; }

        public int? sd_vint { get; set; }

        [Column(TypeName = "money")]
        public decimal? sd_vmoney { get; set; }

        public DateTime? sd_vdatetime { get; set; }

        [MaxLength(3500)]
        public byte[] sd_vbinary { get; set; }

        public long? sd_vbigint { get; set; }

        public virtual st__Site st__Site { get; set; }
    }
}
