namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_AuctionType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_AuctionType()
        {
            au__Auction = new HashSet<au__Auction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int at_Id { get; set; }

        public int at_SvcProvId { get; set; }

        public int at_SiteId { get; set; }

        [Required]
        [StringLength(255)]
        public string at_Name { get; set; }

        [StringLength(50)]
        public string at_Code { get; set; }

        public int? at_Ident { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }
    }
}
