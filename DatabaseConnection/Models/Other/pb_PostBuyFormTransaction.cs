namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pb_PostBuyFormTransaction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pt_Id { get; set; }

        public int pt_PostBuyFormDataId { get; set; }

        public int pt_TransactionId { get; set; }

        public int? pt_OrigTransactionId { get; set; }

        [Column(TypeName = "money")]
        public decimal? pt_Amount { get; set; }

        public int? pt_Quantity { get; set; }

        public long? pt_ExternalDealId { get; set; }

        public virtual pb__PostBuyFormData pb__PostBuyFormData { get; set; }

        public virtual tr__Transaction tr__Transaction { get; set; }
    }
}
