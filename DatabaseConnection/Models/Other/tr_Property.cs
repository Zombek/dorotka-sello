namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tr_Property
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tp_Id { get; set; }

        public int tp_TransId { get; set; }

        public int tp_PropId { get; set; }

        public virtual sl_TransProperty sl_TransProperty { get; set; }

        public virtual tr__Transaction tr__Transaction { get; set; }
    }
}
