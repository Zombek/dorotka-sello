namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em_Attachment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int et_Id { get; set; }

        public int et_EmailId { get; set; }

        [Required]
        [StringLength(260)]
        public string et_Filename { get; set; }

        [Required]
        [StringLength(260)]
        public string et_Filepath { get; set; }

        public virtual em__Email em__Email { get; set; }
    }
}
