namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tr_Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tc_Id { get; set; }

        public int tc_TransId { get; set; }

        public DateTime? tc_SentDate { get; set; }

        [StringLength(255)]
        public string tc_SentComment { get; set; }

        public int? tc_SentScore { get; set; }

        public DateTime? tc_ReceivedDate { get; set; }

        [StringLength(255)]
        public string tc_ReceivedComment { get; set; }

        public int? tc_ReceivedScore { get; set; }

        [StringLength(50)]
        public string tc_ExternalId { get; set; }

        public virtual tr__Transaction tr__Transaction { get; set; }
    }
}
