namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ep_ExternalProgram
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ep_ExternalProgram()
        {
            ep__Registration = new HashSet<ep__Registration>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ep_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string ep_Name { get; set; }

        [Required]
        [StringLength(50)]
        public string ep_ProgId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ep__Registration> ep__Registration { get; set; }
    }
}
