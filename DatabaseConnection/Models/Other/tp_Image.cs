namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tp_Image
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ti_Id { get; set; }

        public int ti_RepositoryId { get; set; }

        public int ti_HtmlTemplateId { get; set; }

        public virtual im__Image im__Image { get; set; }

        public virtual tp__HtmlTemplate tp__HtmlTemplate { get; set; }
    }
}
