namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_Payment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ap_Id { get; set; }

        public int ap_AuctionId { get; set; }

        public int ap_OptionId { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual st_Payment st_Payment { get; set; }
    }
}
