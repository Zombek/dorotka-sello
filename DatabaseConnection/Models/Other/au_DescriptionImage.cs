namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_DescriptionImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int di_Id { get; set; }

        public int di_RepositoryId { get; set; }

        public int di_AuctionId { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual im__Image im__Image { get; set; }
    }
}
