namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class log__Sync
    {
        [Key]
        public int log_Id { get; set; }

        public double log_JobId { get; set; }

        public DateTime log_Date { get; set; }

        public int log_UserId { get; set; }

        public int? log_ObjType { get; set; }

        public int? log_ObjId { get; set; }

        [Required]
        [StringLength(1000)]
        public string log_Text { get; set; }

        public int log_Status { get; set; }

        public int log_Level { get; set; }
    }
}
