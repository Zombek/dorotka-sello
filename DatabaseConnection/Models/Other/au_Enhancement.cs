namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class au_Enhancement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ae_Id { get; set; }

        public int ae_AuctionId { get; set; }

        public int ae_OptionId { get; set; }

        public virtual au__Auction au__Auction { get; set; }

        public virtual st_Enhancement st_Enhancement { get; set; }
    }
}
