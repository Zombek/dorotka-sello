namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ep__Registration
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ep__Registration()
        {
            ep_RegData = new HashSet<ep_RegData>();
            ep_UserMapping = new HashSet<ep_UserMapping>();
            it__Item = new HashSet<it__Item>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pr_Id { get; set; }

        public int pr_ProgramTypeId { get; set; }

        public DateTime pr_RegDate { get; set; }

        public bool pr_Default { get; set; }

        [Required]
        [StringLength(50)]
        public string pr_Descr { get; set; }

        public DateTime? pr_ItemSynch { get; set; }

        [Required]
        [StringLength(32)]
        public string pr_EPCALastHash { get; set; }

        public virtual ep_ExternalProgram ep_ExternalProgram { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ep_RegData> ep_RegData { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ep_UserMapping> ep_UserMapping { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it__Item> it__Item { get; set; }
    }
}
