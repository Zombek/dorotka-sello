namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ep_UserMapping
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int um_Id { get; set; }

        public int um_EPRegistrationId { get; set; }

        public int? um_FhUserId { get; set; }

        [Required]
        [StringLength(51)]
        public string um_EpUser { get; set; }

        [StringLength(100)]
        public string um_EpUserPwd { get; set; }

        [StringLength(50)]
        public string um_EpRole { get; set; }

        [StringLength(100)]
        public string um_EpRolePwd { get; set; }

        public int? um_EpUserId { get; set; }

        public virtual ep__Registration ep__Registration { get; set; }

        public virtual sl_User sl_User { get; set; }
    }
}
