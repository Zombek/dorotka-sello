namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pd_Permission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pp_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string pp_Module { get; set; }

        [Required]
        [StringLength(120)]
        public string pp_Name { get; set; }

        public bool pp_AccountPermission { get; set; }
    }
}
