namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class xit_Ewid
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int itx_IdSource { get; set; }

        public int? itx_Level { get; set; }

        public int? itx_IdFrom { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string itx_Entry { get; set; }

        public int? itx_Rank { get; set; }
    }
}
