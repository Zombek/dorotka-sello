namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class rn_ReturnedNumber
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int rn_Id { get; set; }

        public int rn_SendingNumbersPoolId { get; set; }

        [Required]
        [StringLength(50)]
        public string rn_Number { get; set; }

        public virtual sl_SendingNumbersPool sl_SendingNumbersPool { get; set; }
    }
}
