namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_Payment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_Payment()
        {
            au_Payment = new HashSet<au_Payment>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int sp_Id { get; set; }

        public int sp_SvcProvId { get; set; }

        public int sp_SiteId { get; set; }

        [Required]
        [StringLength(255)]
        public string sp_Name { get; set; }

        public int? sp_Ident { get; set; }

        [StringLength(50)]
        public string sp_Code { get; set; }

        public bool? sp_Expired { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Payment> au_Payment { get; set; }
    }
}
