namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw__Konfiguracja
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string vwcg_Section { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int vwcg_User { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int vwcg_Service { get; set; }

        [Column(TypeName = "image")]
        public byte[] vwcg_Data { get; set; }

        [Column(TypeName = "image")]
        public byte[] vwcg_FiltersData { get; set; }
    }
}
