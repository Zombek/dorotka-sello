namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("__Tabele")]
    public partial class C__Tabele
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string Nazwa_tabeli { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string Przedrostek_atrybutu { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(255)]
        public string Opis { get; set; }

        [StringLength(20)]
        public string Autor { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool Skonczona { get; set; }

        [StringLength(100)]
        public string Duplikaty { get; set; }
    }
}
