namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em_Signature
    {
        [Key]
        public int es_Id { get; set; }

        public int es_RegId { get; set; }

        public int es_SiteId { get; set; }

        [Column(TypeName = "text")]
        public string es_Text { get; set; }
    }
}
