namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class xit_Szum
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int szit_Id { get; set; }

        public int szit_ParamId { get; set; }

        [Required]
        [StringLength(50)]
        public string szit_Item { get; set; }

        public virtual insx_Parametr insx_Parametr { get; set; }
    }
}
