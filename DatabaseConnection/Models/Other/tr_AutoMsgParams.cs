namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tr_AutoMsgParams
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tm_Id { get; set; }

        public int? tm_TransPayment { get; set; }

        public int? tm_TransOnDeliveryPayment { get; set; }

        public int? tm_PackageSent { get; set; }

        public int? tm_PackageOnDeliverySent { get; set; }

        public int? tm_TransBegin { get; set; }

        public int? tm_TransGroupCreated { get; set; }

        public int? tm_Account { get; set; }

        public bool tm_Disabled { get; set; }

        public int? tm_TransOverdue { get; set; }

        public int? tm_TransSetOnDeliveryPayment { get; set; }

        public int? tm_PackageCreated { get; set; }

        public int? tm_TransDeliveryAddressChanged { get; set; }

        public virtual em_Template em_Template { get; set; }

        public virtual em_Template em_Template1 { get; set; }

        public virtual em_Template em_Template2 { get; set; }

        public virtual em_Template em_Template3 { get; set; }

        public virtual em_Template em_Template4 { get; set; }

        public virtual em_Template em_Template5 { get; set; }

        public virtual em_Template em_Template6 { get; set; }

        public virtual em_Template em_Template7 { get; set; }

        public virtual em_Template em_Template8 { get; set; }
    }
}
