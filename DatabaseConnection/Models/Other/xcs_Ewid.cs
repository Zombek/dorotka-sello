namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class xcs_Ewid
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int csx_IdSource { get; set; }

        public int? csx_Level { get; set; }

        public int? csx_IdFrom { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string csx_Entry { get; set; }

        public int? csx_Rank { get; set; }
    }
}
