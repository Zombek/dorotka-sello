namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class em_Account
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ea_Id { get; set; }

        public int ea_UserId { get; set; }

        [Required]
        [StringLength(50)]
        public string ea_AccountName { get; set; }

        [StringLength(50)]
        public string ea_UserName { get; set; }

        [StringLength(50)]
        public string ea_UserAddress { get; set; }

        [Required]
        [StringLength(50)]
        public string ea_Login { get; set; }

        [Required]
        [StringLength(256)]
        public string ea_Password { get; set; }

        [StringLength(50)]
        public string ea_POP3 { get; set; }

        public int ea_POP3Port { get; set; }

        public bool ea_POP3SSL { get; set; }

        [StringLength(50)]
        public string ea_SMTP { get; set; }

        public int ea_SMTPPort { get; set; }

        public bool ea_SMTPAuth { get; set; }

        public bool ea_SMTPSSL { get; set; }

        public bool ea_Disabled { get; set; }

        public bool ea_LeaveOnServer { get; set; }

        public bool ea_Default { get; set; }

        public int ea_POP3Encryption { get; set; }

        public int ea_SMTPEncryption { get; set; }

        public virtual sl_User sl_User { get; set; }
    }
}
