namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pb__PostBuyFormData
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pb__PostBuyFormData()
        {
            adr__Address = new HashSet<adr__Address>();
            pb_PostBuyFormTransaction = new HashSet<pb_PostBuyFormTransaction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pb_Id { get; set; }

        public long pb_TransactionId { get; set; }

        public int pb_CustomerId { get; set; }

        [Column(TypeName = "money")]
        public decimal? pb_Amount { get; set; }

        [Column(TypeName = "money")]
        public decimal? pb_PostageAmount { get; set; }

        public int pb_Invoice { get; set; }

        [Column(TypeName = "ntext")]
        public string pb_MessageToSeller { get; set; }

        public DateTime? pb_InvoiceDataFormCreatedDate { get; set; }

        public DateTime? pb_ShipmentAddressFormCreatedDate { get; set; }

        [StringLength(100)]
        public string pb_PayType { get; set; }

        public long? pb_PayId { get; set; }

        [StringLength(50)]
        public string pb_PayStatus { get; set; }

        public DateTime? pb_DateInit { get; set; }

        public DateTime? pb_DateRecv { get; set; }

        public DateTime? pb_DateCancel { get; set; }

        public int? pb_ShipmentId { get; set; }

        public DateTime? pb_GdAddressFormCreatedDate { get; set; }

        [Column(TypeName = "ntext")]
        public string pb_GdAdditionalInfo { get; set; }

        [Column(TypeName = "money")]
        public decimal? pb_PaymentAmount { get; set; }

        public bool pb_IsSurcharge { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adr__Address> adr__Address { get; set; }

        public virtual pb__PostBuyFormData pb__PostBuyFormData1 { get; set; }

        public virtual pb__PostBuyFormData pb__PostBuyFormData2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pb_PostBuyFormTransaction> pb_PostBuyFormTransaction { get; set; }
    }
}
