namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_SendingNumbersPool
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_SendingNumbersPool()
        {
            pc__Package = new HashSet<pc__Package>();
            rn_ReturnedNumber = new HashSet<rn_ReturnedNumber>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int np_Id { get; set; }

        public int np_Type { get; set; }

        [Required]
        [StringLength(25)]
        public string np_From { get; set; }

        [Required]
        [StringLength(25)]
        public string np_To { get; set; }

        [Required]
        [StringLength(25)]
        public string np_CodePrefix { get; set; }

        [Required]
        [StringLength(25)]
        public string np_CodeSuffix { get; set; }

        [Required]
        [StringLength(255)]
        public string np_Notes { get; set; }

        [Required]
        [StringLength(25)]
        public string np_NextNumber { get; set; }

        public bool np_Active { get; set; }

        public DateTime? np_ContractDate { get; set; }

        [Required]
        [StringLength(50)]
        public string np_ContractWith { get; set; }

        [Required]
        [StringLength(50)]
        public string np_SendingPlace { get; set; }

        public int np_CodeType { get; set; }

        public int np_NumbersLeft { get; set; }

        [Required]
        [StringLength(50)]
        public string np_ContractNumber { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<rn_ReturnedNumber> rn_ReturnedNumber { get; set; }

        public virtual sl_SendingNumbersPoolType sl_SendingNumbersPoolType { get; set; }
    }
}
