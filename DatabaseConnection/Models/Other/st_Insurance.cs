namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class st_Insurance
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public st_Insurance()
        {
            au_Ebay = new HashSet<au_Ebay>();
            au_Ebay1 = new HashSet<au_Ebay>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int si_Id { get; set; }

        public int si_SvcProvId { get; set; }

        public int si_SiteId { get; set; }

        [StringLength(255)]
        public string si_Name { get; set; }

        public int? si_Ident { get; set; }

        [StringLength(50)]
        public string si_Code { get; set; }

        public bool si_Expired { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Ebay> au_Ebay { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au_Ebay> au_Ebay1 { get; set; }
    }
}
