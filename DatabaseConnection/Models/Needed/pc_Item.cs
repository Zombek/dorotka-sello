namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pc_Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pt_Id { get; set; }

        public int pt_PackageId { get; set; }

        public int? pt_TransItemId { get; set; }

        public int? pt_ItemId { get; set; }

        public int pt_Quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal pt_UnitWeight { get; set; }

        public virtual it__Item it__Item { get; set; }

        public virtual pc__Package pc__Package { get; set; }

        public virtual tr_Item tr_Item { get; set; }
    }
}
