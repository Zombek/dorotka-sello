namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs__Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cs__Customer()
        {
            adr__Address = new HashSet<adr__Address>();
            cs_CustEmail = new HashSet<cs_CustEmail>();
            cs_CustIM = new HashSet<cs_CustIM>();
            cs_CustomerProp = new HashSet<cs_CustomerProp>();
            cs_CustPhone = new HashSet<cs_CustPhone>();
            em__Email = new HashSet<em__Email>();
            pc__Package = new HashSet<pc__Package>();
            tr__Transaction = new HashSet<tr__Transaction>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int cs_Id { get; set; }

        [Required]
        [StringLength(20)]
        public string cs_Symbol { get; set; }

        [Required]
        [StringLength(129)]
        public string cs_Name { get; set; }

        [Required]
        [StringLength(64)]
        public string cs_Surname { get; set; }

        public int? cs_SvcProvId { get; set; }

        [Required]
        [StringLength(64)]
        public string cs_Nick { get; set; }

        public int cs_Rank { get; set; }

        public int? cs_ExternalCustomerId { get; set; }

        public int? cs_RegistrationId { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string cs_Description { get; set; }

        public int? cs_GroupId { get; set; }

        [Required]
        [StringLength(255)]
        public string cs_WWW { get; set; }

        [StringLength(255)]
        public string cs_ExtServiceId { get; set; }

        [Required]
        [StringLength(255)]
        public string cs_Company { get; set; }

        [Required]
        [StringLength(20)]
        public string cs_NIP { get; set; }

        public bool cs_Linked { get; set; }

        public bool cs_Activated { get; set; }

        public int? cs_AnonimisedByUser { get; set; }

        public DateTime? cs_AnonimisationDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adr__Address> adr__Address { get; set; }

        public virtual sl_CustomerGroup sl_CustomerGroup { get; set; }

        public virtual sl_ServiceProvider sl_ServiceProvider { get; set; }

        public virtual sl_User sl_User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_CustEmail> cs_CustEmail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_CustIM> cs_CustIM { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_CustomerProp> cs_CustomerProp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cs_CustPhone> cs_CustPhone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em__Email> em__Email { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr__Transaction> tr__Transaction { get; set; }
    }
}
