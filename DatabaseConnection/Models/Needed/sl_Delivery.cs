namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_Delivery
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_Delivery()
        {
            ds_DeliveryToShippingMap = new HashSet<ds_DeliveryToShippingMap>();
            pc__Package = new HashSet<pc__Package>();
            pc__Package1 = new HashSet<pc__Package>();
            pf_Delivery = new HashSet<pf_Delivery>();
            tr_Default = new HashSet<tr_Default>();
            tr__Transaction = new HashSet<tr__Transaction>();
            tr__Transaction1 = new HashSet<tr__Transaction>();
            tr_Default1 = new HashSet<tr_Default>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int dm_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string dm_Name { get; set; }

        public int dm_DelivererId { get; set; }

        public bool dm_PayOnDelivery { get; set; }

        public bool dm_Priority { get; set; }

        public bool dm_Assurance { get; set; }

        public bool dm_Carrefully { get; set; }

        public bool dm_HugeGabarit { get; set; }

        public bool dm_PosteRestante { get; set; }

        public bool dm_ReceiveConfirmation { get; set; }

        public int? dm_SendingNumbersPoolTypeId { get; set; }

        public int dm_SynchronizationMode { get; set; }

        public bool dm_KopiujWartoscPobraniaDoWartosciPaczki { get; set; }

        public bool dm_IsActive { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ds_DeliveryToShippingMap> ds_DeliveryToShippingMap { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pf_Delivery> pf_Delivery { get; set; }

        public virtual sl_Deliverer sl_Deliverer { get; set; }

        public virtual sl_SendingNumbersPoolType sl_SendingNumbersPoolType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Default> tr_Default { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr__Transaction> tr__Transaction { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr__Transaction> tr__Transaction1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Default> tr_Default1 { get; set; }
    }
}
