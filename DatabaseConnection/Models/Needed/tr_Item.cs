namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tr_Item
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tr_Item()
        {
            pc_Item = new HashSet<pc_Item>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int tt_Id { get; set; }

        public int tt_TransId { get; set; }

        public int? tt_ItemId { get; set; }

        [StringLength(100)]
        public string tt_Name { get; set; }

        public int tt_Quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal tt_Price { get; set; }

        public int? tt_OrigTransId { get; set; }

        [Column(TypeName = "money")]
        public decimal tt_UnitWeight { get; set; }

        public virtual it__Item it__Item { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc_Item> pc_Item { get; set; }

        public virtual tr__Transaction tr__Transaction { get; set; }

        public virtual tr__Transaction tr__Transaction1 { get; set; }
    }
}
