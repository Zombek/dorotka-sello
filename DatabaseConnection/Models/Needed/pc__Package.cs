namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class pc__Package
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pc__Package()
        {
            adr__Address = new HashSet<adr__Address>();
            em__Email = new HashSet<em__Email>();
            pc_Error = new HashSet<pc_Error>();
            pc_History = new HashSet<pc_History>();
            pc_SendingListPackage = new HashSet<pc_SendingListPackage>();
            pc_Item = new HashSet<pc_Item>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pc_Id { get; set; }

        public int pc_SymbolIndex { get; set; }

        public int pc_SymbolYear { get; set; }

        public int pc_CustomerId { get; set; }

        [Column(TypeName = "money")]
        public decimal pc_Charge { get; set; }

        [Required]
        [StringLength(3)]
        public string pc_Currency { get; set; }

        public bool pc_PayOnDelivery { get; set; }

        public DateTime pc_CreationDate { get; set; }

        public DateTime? pc_SendingDate { get; set; }

        public DateTime? pc_ReturnDate { get; set; }

        public int? pc_DelivererId { get; set; }

        public int? pc_DeliveryId { get; set; }

        public DateTime? pc_LockDate { get; set; }

        [Required]
        [StringLength(50)]
        public string pc_Document { get; set; }

        [Required]
        [StringLength(255)]
        public string pc_StatusLink { get; set; }

        [Required]
        [StringLength(255)]
        public string pc_Remarks { get; set; }

        public DateTime? pc_LastModified { get; set; }

        public int? pc_OwnerId { get; set; }

        public bool pc_OnStorageList { get; set; }

        public bool pc_ChangeLock { get; set; }

        [Column(TypeName = "money")]
        public decimal? pc_Value { get; set; }

        public int? pc_TransId { get; set; }

        public int pc_RegId { get; set; }

        public int? pc_SiteId { get; set; }

        [Required]
        [StringLength(3)]
        public string pc_BaseCurrency { get; set; }

        public bool pc_SendingNumberFromPool { get; set; }

        public int? pc_SendingNumberPoolId { get; set; }

        [Column(TypeName = "money")]
        public decimal? pc_Weight { get; set; }

        [Column(TypeName = "money")]
        public decimal? pc_SendCharge { get; set; }

        public bool pc_AutoStatusLink { get; set; }

        public bool pc_EvaluateWeightFromItems { get; set; }

        public int? pc_NotesId { get; set; }

        public bool pc_AutoMailDisabled { get; set; }

        [StringLength(10)]
        public string pc_SizeType { get; set; }

        public int? pc_Status { get; set; }

        [StringLength(20)]
        public string pc_CustomerDeliveringCode { get; set; }

        public DateTime? pc_ExternalServicePaymentDate { get; set; }

        public Guid? pc_GUID { get; set; }

        public DateTime? pc_DeliveryDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int pc_SentStatus { get; set; }

        [Required]
        [StringLength(20)]
        public string pc_ExternalServiceId { get; set; }

        public int pc_SendingNumberSyncState { get; set; }

        public int pc_SizeHeight { get; set; }

        public int pc_SizeWidth { get; set; }

        public int pc_SizeDepth { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adr__Address> adr__Address { get; set; }

        public virtual cs__Customer cs__Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<em__Email> em__Email { get; set; }

        public virtual reg__Registration reg__Registration { get; set; }

        public virtual reg_Site reg_Site { get; set; }

        public virtual sl_Deliverer sl_Deliverer { get; set; }

        public virtual sl_Delivery sl_Delivery { get; set; }

        public virtual sl_Delivery sl_Delivery1 { get; set; }

        public virtual sl_SendingNumbersPool sl_SendingNumbersPool { get; set; }

        public virtual sl_User sl_User { get; set; }

        public virtual tr__Transaction tr__Transaction { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc_Error> pc_Error { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc_History> pc_History { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc_SendingListPackage> pc_SendingListPackage { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc_Item> pc_Item { get; set; }
    }
}
