namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class it__Item
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public it__Item()
        {
            au__Auction = new HashSet<au__Auction>();
            it_AuctionTemplate = new HashSet<it_AuctionTemplate>();
            it_CustomAttributes = new HashSet<it_CustomAttributes>();
            it_ExternalCustomAttributes = new HashSet<it_ExternalCustomAttributes>();
            it_ItemPictures = new HashSet<it_ItemPictures>();
            it_ItemProp = new HashSet<it_ItemProp>();
            it_ItemSite = new HashSet<it_ItemSite>();
            pc_Item = new HashSet<pc_Item>();
            tr_Item = new HashSet<tr_Item>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int it_Id { get; set; }

        [Required]
        [StringLength(100)]
        public string it_Name { get; set; }

        [Required]
        [StringLength(20)]
        public string it_Symbol { get; set; }

        [Column(TypeName = "money")]
        public decimal it_Price { get; set; }

        public int? it_GroupId { get; set; }

        [Column(TypeName = "money")]
        public decimal it_Stock { get; set; }

        [Column(TypeName = "money")]
        public decimal it_OnAuctions { get; set; }

        [Column(TypeName = "money")]
        public decimal it_Reserved { get; set; }

        [Column(TypeName = "money")]
        public decimal it_Min { get; set; }

        public bool it_Linked { get; set; }

        public int? it_ExternalItemId { get; set; }

        public int? it_RegistrationId { get; set; }

        public bool it_Private { get; set; }

        [Required]
        [StringLength(255)]
        public string it_WWW { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string it_Descr { get; set; }

        [Column(TypeName = "money")]
        public decimal it_CostPrice { get; set; }

        [Column(TypeName = "money")]
        public decimal it_Weight { get; set; }

        [Required]
        [StringLength(255)]
        public string it_Producer { get; set; }

        public bool it_SynchEPPrice { get; set; }

        public bool it_ControlStock { get; set; }

        public bool it_HasDescription2 { get; set; }

        [Required]
        [StringLength(20)]
        public string it_EAN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<au__Auction> au__Auction { get; set; }

        public virtual ep__Registration ep__Registration { get; set; }

        public virtual sl_ItemGroup sl_ItemGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_AuctionTemplate> it_AuctionTemplate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_CustomAttributes> it_CustomAttributes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_ExternalCustomAttributes> it_ExternalCustomAttributes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_ItemPictures> it_ItemPictures { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_ItemProp> it_ItemProp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<it_ItemSite> it_ItemSite { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc_Item> pc_Item { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Item> tr_Item { get; set; }
    }
}
