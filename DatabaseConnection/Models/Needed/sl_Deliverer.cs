namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class sl_Deliverer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sl_Deliverer()
        {
            pc__Package = new HashSet<pc__Package>();
            pc_SendingList = new HashSet<pc_SendingList>();
            sl_Delivery = new HashSet<sl_Delivery>();
            tr__Transaction = new HashSet<tr__Transaction>();
            tr_Default = new HashSet<tr_Default>();
            tr_Default1 = new HashSet<tr_Default>();
        }

        public sl_Deliverer(string name, int id)
        {
            dr_Id = id;
            dr_Name = name;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int dr_Id { get; set; }

        [Required]
        [StringLength(50)]
        public string dr_Name { get; set; }

        [StringLength(255)]
        public string dr_StatusLink { get; set; }

        public int dr_AllegroOperatorId { get; set; }

        [Required]
        [StringLength(50)]
        public string dr_AllegroOperatorName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc__Package> pc__Package { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pc_SendingList> pc_SendingList { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<sl_Delivery> sl_Delivery { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr__Transaction> tr__Transaction { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Default> tr_Default { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tr_Default> tr_Default1 { get; set; }
    }
}
