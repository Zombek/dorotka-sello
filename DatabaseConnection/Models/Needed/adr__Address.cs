namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class adr__Address
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int adr_Id { get; set; }

        public int? adr_Type { get; set; }

        [Required]
        [StringLength(129)]
        public string adr_Name { get; set; }

        [Required]
        [StringLength(64)]
        public string adr_Address1 { get; set; }

        [Required]
        [StringLength(64)]
        public string adr_Address2 { get; set; }

        [Required]
        [StringLength(32)]
        public string adr_ZipCode { get; set; }

        [Required]
        [StringLength(64)]
        public string adr_City { get; set; }

        [Required]
        [StringLength(50)]
        public string adr_Country { get; set; }

        public int? adr_TransId { get; set; }

        public int? adr_PackageId { get; set; }

        public int? adr_CustomerId { get; set; }

        public int? adr_PostBuyFormId { get; set; }

        [Required]
        [StringLength(255)]
        public string adr_Company { get; set; }

        [Required]
        [StringLength(20)]
        public string adr_NIP { get; set; }

        [Required]
        [StringLength(50)]
        public string adr_PhoneNumber { get; set; }

        [StringLength(50)]
        public string adr_Email { get; set; }

        public virtual cs__Customer cs__Customer { get; set; }

        public virtual pb__PostBuyFormData pb__PostBuyFormData { get; set; }

        public virtual pc__Package pc__Package { get; set; }

        public virtual tr__Transaction tr__Transaction { get; set; }
    }
}
