namespace DatabaseConnection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class cs_CustEmail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ce_Id { get; set; }

        public int ce_CustomerId { get; set; }

        [Required]
        [StringLength(255)]
        public string ce_email { get; set; }

        public bool ce_default { get; set; }

        public virtual cs__Customer cs__Customer { get; set; }
    }
}
