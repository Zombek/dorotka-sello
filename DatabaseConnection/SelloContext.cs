namespace DatabaseConnection
{
    using System.Data.Entity;

    public partial class SelloContext : DbContext
    {
        public SelloContext()
            : base("DbContext")
        {
        }

        public virtual DbSet<C__BeforeDropOldTables> C__BeforeDropOldTables { get; set; }
        public virtual DbSet<adr__Address> adr__Address { get; set; }
        public virtual DbSet<au__Auction> au__Auction { get; set; }
        public virtual DbSet<au_Allegro> au_Allegro { get; set; }
        public virtual DbSet<au_CategorySpecific> au_CategorySpecific { get; set; }
        public virtual DbSet<au_DescriptionImage> au_DescriptionImage { get; set; }
        public virtual DbSet<au_Ebay> au_Ebay { get; set; }
        public virtual DbSet<au_Enhancement> au_Enhancement { get; set; }
        public virtual DbSet<au_Features> au_Features { get; set; }
        public virtual DbSet<au_Note> au_Note { get; set; }
        public virtual DbSet<au_Payment> au_Payment { get; set; }
        public virtual DbSet<au_PendingChanges> au_PendingChanges { get; set; }
        public virtual DbSet<au_Picture> au_Picture { get; set; }
        public virtual DbSet<au_Shipping> au_Shipping { get; set; }
        public virtual DbSet<au_TransactionProperty> au_TransactionProperty { get; set; }
        public virtual DbSet<cs__Customer> cs__Customer { get; set; }
        public virtual DbSet<cs_CustEmail> cs_CustEmail { get; set; }
        public virtual DbSet<cs_CustIM> cs_CustIM { get; set; }
        public virtual DbSet<cs_CustomerProp> cs_CustomerProp { get; set; }
        public virtual DbSet<cs_CustPhone> cs_CustPhone { get; set; }
        public virtual DbSet<ctx_UserSite> ctx_UserSite { get; set; }
        public virtual DbSet<ds_DeliveryToShippingMap> ds_DeliveryToShippingMap { get; set; }
        public virtual DbSet<em__Email> em__Email { get; set; }
        public virtual DbSet<em_Account> em_Account { get; set; }
        public virtual DbSet<em_Archive> em_Archive { get; set; }
        public virtual DbSet<em_Attachment> em_Attachment { get; set; }
        public virtual DbSet<em_Rule> em_Rule { get; set; }
        public virtual DbSet<em_RuleWord> em_RuleWord { get; set; }
        public virtual DbSet<em_Signature> em_Signature { get; set; }
        public virtual DbSet<em_Source> em_Source { get; set; }
        public virtual DbSet<em_Template> em_Template { get; set; }
        public virtual DbSet<ep__Registration> ep__Registration { get; set; }
        public virtual DbSet<ep_ExternalProgram> ep_ExternalProgram { get; set; }
        public virtual DbSet<ep_UserMapping> ep_UserMapping { get; set; }
        public virtual DbSet<fp__ParamsFTP> fp__ParamsFTP { get; set; }
        public virtual DbSet<gt__Obiekt> gt__Obiekt { get; set; }
        public virtual DbSet<gt_Atrybut> gt_Atrybut { get; set; }
        public virtual DbSet<gt_Definicja> gt_Definicja { get; set; }
        public virtual DbSet<gt_Transformacja> gt_Transformacja { get; set; }
        public virtual DbSet<gt_TransRodzaj> gt_TransRodzaj { get; set; }
        public virtual DbSet<im__Image> im__Image { get; set; }
        public virtual DbSet<ins_blokada> ins_blokada { get; set; }
        public virtual DbSet<insx_Parametr> insx_Parametr { get; set; }
        public virtual DbSet<it__Item> it__Item { get; set; }
        public virtual DbSet<it_AuctionTemplate> it_AuctionTemplate { get; set; }
        public virtual DbSet<it_CustomAttributes> it_CustomAttributes { get; set; }
        public virtual DbSet<it_ExternalCustomAttributes> it_ExternalCustomAttributes { get; set; }
        public virtual DbSet<it_ExternalCustomAttributesDict> it_ExternalCustomAttributesDict { get; set; }
        public virtual DbSet<it_ItemPictures> it_ItemPictures { get; set; }
        public virtual DbSet<it_ItemProp> it_ItemProp { get; set; }
        public virtual DbSet<it_ItemSite> it_ItemSite { get; set; }
        public virtual DbSet<jb__Job> jb__Job { get; set; }
        public virtual DbSet<jb_Message> jb_Message { get; set; }
        public virtual DbSet<log__Sync> log__Sync { get; set; }
        public virtual DbSet<nm_IgnoredMessages> nm_IgnoredMessages { get; set; }
        public virtual DbSet<nm_NotifierMessage> nm_NotifierMessage { get; set; }
        public virtual DbSet<nt_Note> nt_Note { get; set; }
        public virtual DbSet<pb__PostBuyFormData> pb__PostBuyFormData { get; set; }
        public virtual DbSet<pb_PostBuyFormTransaction> pb_PostBuyFormTransaction { get; set; }
        public virtual DbSet<pc__Package> pc__Package { get; set; }
        public virtual DbSet<pc_Error> pc_Error { get; set; }
        public virtual DbSet<pc_History> pc_History { get; set; }
        public virtual DbSet<pc_Item> pc_Item { get; set; }
        public virtual DbSet<pc_SendingList> pc_SendingList { get; set; }
        public virtual DbSet<pc_SendingListPackage> pc_SendingListPackage { get; set; }
        public virtual DbSet<pd__Podmiot> pd__Podmiot { get; set; }
        public virtual DbSet<pd_AutoSyncParams> pd_AutoSyncParams { get; set; }
        public virtual DbSet<pd_InternetParams> pd_InternetParams { get; set; }
        public virtual DbSet<pd_Permission> pd_Permission { get; set; }
        public virtual DbSet<pd_Sesja> pd_Sesja { get; set; }
        public virtual DbSet<pd_UserPermission> pd_UserPermission { get; set; }
        public virtual DbSet<pf_Delivery> pf_Delivery { get; set; }
        public virtual DbSet<pf_PrintForm> pf_PrintForm { get; set; }
        public virtual DbSet<pl_PricingListData> pl_PricingListData { get; set; }
        public virtual DbSet<reg__Registration> reg__Registration { get; set; }
        public virtual DbSet<reg_AfterSalesServiceConditions> reg_AfterSalesServiceConditions { get; set; }
        public virtual DbSet<reg_Data> reg_Data { get; set; }
        public virtual DbSet<reg_JournalDeals> reg_JournalDeals { get; set; }
        public virtual DbSet<reg_Site> reg_Site { get; set; }
        public virtual DbSet<rn_ReturnedNumber> rn_ReturnedNumber { get; set; }
        public virtual DbSet<sl_AuctionGroup> sl_AuctionGroup { get; set; }
        public virtual DbSet<sl_AuctionTemplateGroup> sl_AuctionTemplateGroup { get; set; }
        public virtual DbSet<sl_CommentTemplate> sl_CommentTemplate { get; set; }
        public virtual DbSet<sl_Currency> sl_Currency { get; set; }
        public virtual DbSet<sl_CurrencyBank> sl_CurrencyBank { get; set; }
        public virtual DbSet<sl_CurrencyExchangeRates> sl_CurrencyExchangeRates { get; set; }
        public virtual DbSet<sl_CurrencyExchangeRatesEntry> sl_CurrencyExchangeRatesEntry { get; set; }
        public virtual DbSet<sl_CustomAttributes> sl_CustomAttributes { get; set; }
        public virtual DbSet<sl_CustomCounter> sl_CustomCounter { get; set; }
        public virtual DbSet<sl_CustomerGroup> sl_CustomerGroup { get; set; }
        public virtual DbSet<sl_CustomGlobalAttributes> sl_CustomGlobalAttributes { get; set; }
        public virtual DbSet<sl_CustProperty> sl_CustProperty { get; set; }
        public virtual DbSet<sl_Deliverer> sl_Deliverer { get; set; }
        public virtual DbSet<sl_Delivery> sl_Delivery { get; set; }
        public virtual DbSet<sl_EmailGroup> sl_EmailGroup { get; set; }
        public virtual DbSet<sl_ExportType> sl_ExportType { get; set; }
        public virtual DbSet<sl_InstantMessenger> sl_InstantMessenger { get; set; }
        public virtual DbSet<sl_ItemGroup> sl_ItemGroup { get; set; }
        public virtual DbSet<sl_ItemProperty> sl_ItemProperty { get; set; }
        public virtual DbSet<sl_Language> sl_Language { get; set; }
        public virtual DbSet<sl_MsgTemplate> sl_MsgTemplate { get; set; }
        public virtual DbSet<sl_PackageSynchronizationParams> sl_PackageSynchronizationParams { get; set; }
        public virtual DbSet<sl_PricingList> sl_PricingList { get; set; }
        public virtual DbSet<sl_SendingNumbersPool> sl_SendingNumbersPool { get; set; }
        public virtual DbSet<sl_SendingNumbersPoolType> sl_SendingNumbersPoolType { get; set; }
        public virtual DbSet<sl_ServiceProvider> sl_ServiceProvider { get; set; }
        public virtual DbSet<sl_TransProperty> sl_TransProperty { get; set; }
        public virtual DbSet<sl_User> sl_User { get; set; }
        public virtual DbSet<st__Site> st__Site { get; set; }
        public virtual DbSet<st_AuctionType> st_AuctionType { get; set; }
        public virtual DbSet<st_Category> st_Category { get; set; }
        public virtual DbSet<st_CategorySpecific> st_CategorySpecific { get; set; }
        public virtual DbSet<st_Country> st_Country { get; set; }
        public virtual DbSet<st_Duration> st_Duration { get; set; }
        public virtual DbSet<st_Enhancement> st_Enhancement { get; set; }
        public virtual DbSet<st_Insurance> st_Insurance { get; set; }
        public virtual DbSet<st_Payment> st_Payment { get; set; }
        public virtual DbSet<st_Region> st_Region { get; set; }
        public virtual DbSet<st_Shipping> st_Shipping { get; set; }
        public virtual DbSet<st_SiteData> st_SiteData { get; set; }
        public virtual DbSet<tp__HtmlTemplate> tp__HtmlTemplate { get; set; }
        public virtual DbSet<tp_Image> tp_Image { get; set; }
        public virtual DbSet<tr__Transaction> tr__Transaction { get; set; }
        public virtual DbSet<tr_AutoMsgParams> tr_AutoMsgParams { get; set; }
        public virtual DbSet<tr_Comment> tr_Comment { get; set; }
        public virtual DbSet<tr_Default> tr_Default { get; set; }
        public virtual DbSet<tr_Group> tr_Group { get; set; }
        public virtual DbSet<tr_Item> tr_Item { get; set; }
        public virtual DbSet<tr_Property> tr_Property { get; set; }
        public virtual DbSet<up_UserParams> up_UserParams { get; set; }
        public virtual DbSet<vw__Konfiguracja> vw__Konfiguracja { get; set; }
        public virtual DbSet<xcs_Szum> xcs_Szum { get; set; }
        public virtual DbSet<xit_Szum> xit_Szum { get; set; }
        public virtual DbSet<C__Modyfikacja> C__Modyfikacja { get; set; }
        public virtual DbSet<C__PostUpdate> C__PostUpdate { get; set; }
        public virtual DbSet<C__Slowniki> C__Slowniki { get; set; }
        public virtual DbSet<C__Tabele> C__Tabele { get; set; }
        public virtual DbSet<C__Update> C__Update { get; set; }
        public virtual DbSet<au_EbayReturnPolicy> au_EbayReturnPolicy { get; set; }
        public virtual DbSet<ep_RegData> ep_RegData { get; set; }
        public virtual DbSet<gr__Konfiguracja> gr__Konfiguracja { get; set; }
        public virtual DbSet<gr__KonfiguracjaEx> gr__KonfiguracjaEx { get; set; }
        public virtual DbSet<gt_TransObiekt> gt_TransObiekt { get; set; }
        public virtual DbSet<ins_counter> ins_counter { get; set; }
        public virtual DbSet<ins_ident> ins_ident { get; set; }
        public virtual DbSet<log_Message> log_Message { get; set; }
        public virtual DbSet<nm_Params> nm_Params { get; set; }
        public virtual DbSet<pd_AutoSyncTimes> pd_AutoSyncTimes { get; set; }
        public virtual DbSet<pd_BankAccounts> pd_BankAccounts { get; set; }
        public virtual DbSet<pd_Parametr> pd_Parametr { get; set; }
        public virtual DbSet<st_CategorySpecificDict> st_CategorySpecificDict { get; set; }
        public virtual DbSet<st_ShippingLocation> st_ShippingLocation { get; set; }
        public virtual DbSet<tr_History> tr_History { get; set; }
        public virtual DbSet<xcs_Ewid> xcs_Ewid { get; set; }
        public virtual DbSet<xit_Ewid> xit_Ewid { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<C__BeforeDropOldTables>()
                .Property(e => e.SQLUpdate)
                .IsUnicode(false);

            modelBuilder.Entity<au__Auction>()
                .Property(e => e.au_StartPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au__Auction>()
                .Property(e => e.au_MinPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au__Auction>()
                .Property(e => e.au_BINPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au__Auction>()
                .Property(e => e.au_Currency)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<au__Auction>()
                .Property(e => e.au_HighestBid)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au__Auction>()
                .Property(e => e.au_Fee)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au__Auction>()
                .Property(e => e.au_Commission)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_Picture)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.ap_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_CategorySpecific)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.as_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_Allegro)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.aal_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_DescriptionImage)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.di_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_Ebay)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.aeb_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_Enhancement)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.ae_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_Features)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.fe_ObjectId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_Note1)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.an_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_Payment)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.ap_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_PendingChanges)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.apc_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_Shipping)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.as_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.au_TransactionProperty)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.atp_AuctionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.em__Email)
                .WithOptional(e => e.au__Auction)
                .HasForeignKey(e => e.em_AuctionId);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.it_AuctionTemplate)
                .WithRequired(e => e.au__Auction)
                .HasForeignKey(e => e.ia_AuctionId);

            modelBuilder.Entity<au__Auction>()
                .HasMany(e => e.tr__Transaction)
                .WithOptional(e => e.au__Auction)
                .HasForeignKey(e => e.tr_AuctionId);

            modelBuilder.Entity<au_CategorySpecific>()
                .Property(e => e.as_vmoney)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au_Ebay>()
                .Property(e => e.aeb_InsuranceDomCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au_Ebay>()
                .Property(e => e.aeb_InsuranceIntlCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au_PendingChanges>()
                .Property(e => e.apc_vmoney)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au_Shipping>()
                .Property(e => e.as_Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<au_Shipping>()
                .Property(e => e.as_Cost2)
                .HasPrecision(19, 4);

            modelBuilder.Entity<cs__Customer>()
                .HasMany(e => e.adr__Address)
                .WithOptional(e => e.cs__Customer)
                .HasForeignKey(e => e.adr_CustomerId);

            modelBuilder.Entity<cs__Customer>()
                .HasMany(e => e.cs_CustEmail)
                .WithRequired(e => e.cs__Customer)
                .HasForeignKey(e => e.ce_CustomerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs__Customer>()
                .HasMany(e => e.cs_CustIM)
                .WithRequired(e => e.cs__Customer)
                .HasForeignKey(e => e.cm_CustomerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs__Customer>()
                .HasMany(e => e.cs_CustomerProp)
                .WithRequired(e => e.cs__Customer)
                .HasForeignKey(e => e.cp_CustId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs__Customer>()
                .HasMany(e => e.cs_CustPhone)
                .WithRequired(e => e.cs__Customer)
                .HasForeignKey(e => e.cp_CustomerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs__Customer>()
                .HasMany(e => e.em__Email)
                .WithOptional(e => e.cs__Customer)
                .HasForeignKey(e => e.em_CustomerId);

            modelBuilder.Entity<cs__Customer>()
                .HasMany(e => e.pc__Package)
                .WithRequired(e => e.cs__Customer)
                .HasForeignKey(e => e.pc_CustomerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<cs__Customer>()
                .HasMany(e => e.tr__Transaction)
                .WithRequired(e => e.cs__Customer)
                .HasForeignKey(e => e.tr_CustomerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<em__Email>()
                .Property(e => e.em_UIDL)
                .IsUnicode(false);

            modelBuilder.Entity<em__Email>()
                .Property(e => e.em_MessageId)
                .IsUnicode(false);

            modelBuilder.Entity<em__Email>()
                .Property(e => e.em_Source)
                .IsUnicode(false);

            modelBuilder.Entity<em__Email>()
                .HasMany(e => e.em_Attachment)
                .WithRequired(e => e.em__Email)
                .HasForeignKey(e => e.et_EmailId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<em__Email>()
                .HasMany(e => e.em_Source1)
                .WithRequired(e => e.em__Email)
                .HasForeignKey(e => e.es_EmailId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<em_Account>()
                .Property(e => e.ea_AccountName)
                .IsUnicode(false);

            modelBuilder.Entity<em_Account>()
                .Property(e => e.ea_POP3)
                .IsUnicode(false);

            modelBuilder.Entity<em_Account>()
                .Property(e => e.ea_SMTP)
                .IsUnicode(false);

            modelBuilder.Entity<em_Archive>()
                .Property(e => e.ear_UIDL)
                .IsUnicode(false);

            modelBuilder.Entity<em_Rule>()
                .Property(e => e.er_Name)
                .IsUnicode(false);

            modelBuilder.Entity<em_Rule>()
                .HasMany(e => e.em_RuleWord)
                .WithRequired(e => e.em_Rule)
                .HasForeignKey(e => e.ew_RuleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<em_RuleWord>()
                .Property(e => e.ew_Word)
                .IsUnicode(false);

            modelBuilder.Entity<em_Signature>()
                .Property(e => e.es_Text)
                .IsUnicode(false);

            modelBuilder.Entity<em_Source>()
                .Property(e => e.es_Source)
                .IsUnicode(false);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams)
                .WithOptional(e => e.em_Template)
                .HasForeignKey(e => e.tm_TransPayment);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams1)
                .WithOptional(e => e.em_Template1)
                .HasForeignKey(e => e.tm_TransOnDeliveryPayment);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams2)
                .WithOptional(e => e.em_Template2)
                .HasForeignKey(e => e.tm_PackageSent);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams3)
                .WithOptional(e => e.em_Template3)
                .HasForeignKey(e => e.tm_TransBegin);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams4)
                .WithOptional(e => e.em_Template4)
                .HasForeignKey(e => e.tm_TransGroupCreated);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams5)
                .WithOptional(e => e.em_Template5)
                .HasForeignKey(e => e.tm_PackageOnDeliverySent);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams6)
                .WithOptional(e => e.em_Template6)
                .HasForeignKey(e => e.tm_PackageCreated);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams7)
                .WithOptional(e => e.em_Template7)
                .HasForeignKey(e => e.tm_TransOverdue);

            modelBuilder.Entity<em_Template>()
                .HasMany(e => e.tr_AutoMsgParams8)
                .WithOptional(e => e.em_Template8)
                .HasForeignKey(e => e.tm_TransDeliveryAddressChanged);

            modelBuilder.Entity<ep__Registration>()
                .HasMany(e => e.ep_RegData)
                .WithRequired(e => e.ep__Registration)
                .HasForeignKey(e => e.erd_RegId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ep__Registration>()
                .HasMany(e => e.ep_UserMapping)
                .WithRequired(e => e.ep__Registration)
                .HasForeignKey(e => e.um_EPRegistrationId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ep__Registration>()
                .HasMany(e => e.it__Item)
                .WithOptional(e => e.ep__Registration)
                .HasForeignKey(e => e.it_RegistrationId);

            modelBuilder.Entity<ep_ExternalProgram>()
                .HasMany(e => e.ep__Registration)
                .WithRequired(e => e.ep_ExternalProgram)
                .HasForeignKey(e => e.pr_ProgramTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<gt__Obiekt>()
                .HasMany(e => e.gt_Definicja1)
                .WithRequired(e => e.gt__Obiekt1)
                .HasForeignKey(e => e.gtd_ObiektId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<gt__Obiekt>()
                .HasMany(e => e.gt_Transformacja1)
                .WithRequired(e => e.gt__Obiekt1)
                .HasForeignKey(e => e.gtt_ObiektId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<gt_Definicja>()
                .HasMany(e => e.gt__Obiekt)
                .WithOptional(e => e.gt_Definicja)
                .HasForeignKey(e => e.gto_PodstDefId);

            modelBuilder.Entity<gt_Definicja>()
                .HasMany(e => e.gt_TransObiekt)
                .WithRequired(e => e.gt_Definicja)
                .HasForeignKey(e => e.to_DefinicjaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<gt_Transformacja>()
                .HasMany(e => e.gt__Obiekt)
                .WithOptional(e => e.gt_Transformacja)
                .HasForeignKey(e => e.gto_PodstTransId);

            modelBuilder.Entity<gt_Transformacja>()
                .HasMany(e => e.gt_TransObiekt)
                .WithRequired(e => e.gt_Transformacja)
                .HasForeignKey(e => e.to_TransformacjaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<im__Image>()
                .Property(e => e.im_CheckSum)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<im__Image>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.im__Image)
                .HasForeignKey(e => e.au_ThumbnailId);

            modelBuilder.Entity<im__Image>()
                .HasMany(e => e.au_DescriptionImage)
                .WithRequired(e => e.im__Image)
                .HasForeignKey(e => e.di_RepositoryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<im__Image>()
                .HasMany(e => e.au_Picture)
                .WithOptional(e => e.im__Image)
                .HasForeignKey(e => e.ap_PictureId);

            modelBuilder.Entity<im__Image>()
                .HasMany(e => e.it_ItemPictures)
                .WithRequired(e => e.im__Image)
                .HasForeignKey(e => e.pc_PictureId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<im__Image>()
                .HasMany(e => e.tp_Image)
                .WithRequired(e => e.im__Image)
                .HasForeignKey(e => e.ti_RepositoryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<insx_Parametr>()
                .HasMany(e => e.xcs_Szum)
                .WithRequired(e => e.insx_Parametr)
                .HasForeignKey(e => e.szcs_ParamId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<insx_Parametr>()
                .HasMany(e => e.xit_Szum)
                .WithRequired(e => e.insx_Parametr)
                .HasForeignKey(e => e.szit_ParamId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<it__Item>()
                .Property(e => e.it_Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it__Item>()
                .Property(e => e.it_Stock)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it__Item>()
                .Property(e => e.it_OnAuctions)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it__Item>()
                .Property(e => e.it_Reserved)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it__Item>()
                .Property(e => e.it_Min)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it__Item>()
                .Property(e => e.it_CostPrice)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it__Item>()
                .Property(e => e.it_Weight)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.it__Item)
                .HasForeignKey(e => e.au_ProductId);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.it_AuctionTemplate)
                .WithRequired(e => e.it__Item)
                .HasForeignKey(e => e.ia_ItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.it_CustomAttributes)
                .WithRequired(e => e.it__Item)
                .HasForeignKey(e => e.ia_ItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.it_ExternalCustomAttributes)
                .WithRequired(e => e.it__Item)
                .HasForeignKey(e => e.eca_ItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.it_ItemPictures)
                .WithRequired(e => e.it__Item)
                .HasForeignKey(e => e.pc_ItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.it_ItemProp)
                .WithRequired(e => e.it__Item)
                .HasForeignKey(e => e.ip_ItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.it_ItemSite)
                .WithRequired(e => e.it__Item)
                .HasForeignKey(e => e.is_ItemId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.pc_Item)
                .WithOptional(e => e.it__Item)
                .HasForeignKey(e => e.pt_ItemId);

            modelBuilder.Entity<it__Item>()
                .HasMany(e => e.tr_Item)
                .WithOptional(e => e.it__Item)
                .HasForeignKey(e => e.tt_ItemId);

            modelBuilder.Entity<it_ExternalCustomAttributesDict>()
                .HasMany(e => e.it_ExternalCustomAttributes)
                .WithRequired(e => e.it_ExternalCustomAttributesDict)
                .HasForeignKey(e => e.eca_AttributeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<it_ItemSite>()
                .Property(e => e.is_Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it_ItemSite>()
                .Property(e => e.is_AC_ParamA_L)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it_ItemSite>()
                .Property(e => e.is_AC_ParamA_M)
                .HasPrecision(19, 4);

            modelBuilder.Entity<it_ItemSite>()
                .Property(e => e.is_AC_ParamB)
                .HasPrecision(19, 4);

            modelBuilder.Entity<nm_NotifierMessage>()
                .HasMany(e => e.nm_IgnoredMessages)
                .WithRequired(e => e.nm_NotifierMessage)
                .HasForeignKey(e => e.nm_MessageId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<pb__PostBuyFormData>()
                .Property(e => e.pb_Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pb__PostBuyFormData>()
                .Property(e => e.pb_PostageAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pb__PostBuyFormData>()
                .Property(e => e.pb_PaymentAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pb__PostBuyFormData>()
                .HasMany(e => e.adr__Address)
                .WithOptional(e => e.pb__PostBuyFormData)
                .HasForeignKey(e => e.adr_PostBuyFormId);

            modelBuilder.Entity<pb__PostBuyFormData>()
                .HasOptional(e => e.pb__PostBuyFormData1)
                .WithRequired(e => e.pb__PostBuyFormData2);

            modelBuilder.Entity<pb__PostBuyFormData>()
                .HasMany(e => e.pb_PostBuyFormTransaction)
                .WithRequired(e => e.pb__PostBuyFormData)
                .HasForeignKey(e => e.pt_PostBuyFormDataId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<pb_PostBuyFormTransaction>()
                .Property(e => e.pt_Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pc__Package>()
                .Property(e => e.pc_Charge)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pc__Package>()
                .Property(e => e.pc_Currency)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<pc__Package>()
                .Property(e => e.pc_Value)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pc__Package>()
                .Property(e => e.pc_BaseCurrency)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<pc__Package>()
                .Property(e => e.pc_Weight)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pc__Package>()
                .Property(e => e.pc_SendCharge)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pc__Package>()
                .Property(e => e.pc_SizeType)
                .IsUnicode(false);

            modelBuilder.Entity<pc__Package>()
                .Property(e => e.pc_ExternalServiceId)
                .IsUnicode(false);

            modelBuilder.Entity<pc__Package>()
                .HasMany(e => e.adr__Address)
                .WithOptional(e => e.pc__Package)
                .HasForeignKey(e => e.adr_PackageId);

            modelBuilder.Entity<pc__Package>()
                .HasMany(e => e.em__Email)
                .WithOptional(e => e.pc__Package)
                .HasForeignKey(e => e.em_PackageId);

            modelBuilder.Entity<pc__Package>()
                .HasMany(e => e.pc_Error)
                .WithRequired(e => e.pc__Package)
                .HasForeignKey(e => e.pe_ParentId);

            modelBuilder.Entity<pc__Package>()
                .HasMany(e => e.pc_History)
                .WithRequired(e => e.pc__Package)
                .HasForeignKey(e => e.ph_PackageId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<pc__Package>()
                .HasMany(e => e.pc_SendingListPackage)
                .WithRequired(e => e.pc__Package)
                .HasForeignKey(e => e.pp_PackageId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<pc__Package>()
                .HasMany(e => e.pc_Item)
                .WithRequired(e => e.pc__Package)
                .HasForeignKey(e => e.pt_PackageId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<pc_Item>()
                .Property(e => e.pt_UnitWeight)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pd__Podmiot>()
                .Property(e => e.pd_BaseCurrency)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<pd__Podmiot>()
                .HasMany(e => e.pd_BankAccounts)
                .WithRequired(e => e.pd__Podmiot)
                .HasForeignKey(e => e.pd_EntityId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<pd_Sesja>()
                .Property(e => e.ss_hid)
                .IsUnicode(false);

            modelBuilder.Entity<pf_PrintForm>()
                .HasMany(e => e.pf_Delivery)
                .WithRequired(e => e.pf_PrintForm)
                .HasForeignKey(e => e.pd_PrintFormId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<pl_PricingListData>()
                .Property(e => e.pd_Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<pl_PricingListData>()
                .Property(e => e.pd_Cost2)
                .HasPrecision(19, 4);

            modelBuilder.Entity<reg__Registration>()
                .Property(e => e.reg_Accesskey)
                .IsUnicode(false);

            modelBuilder.Entity<reg__Registration>()
                .Property(e => e.reg_Version)
                .IsUnicode(false);

            modelBuilder.Entity<reg__Registration>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.reg__Registration)
                .HasForeignKey(e => e.au_RegId);

            modelBuilder.Entity<reg__Registration>()
                .HasMany(e => e.pc__Package)
                .WithRequired(e => e.reg__Registration)
                .HasForeignKey(e => e.pc_RegId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<reg__Registration>()
                .HasMany(e => e.reg_AfterSalesServiceConditions)
                .WithRequired(e => e.reg__Registration)
                .HasForeignKey(e => e.assc_RegId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<reg__Registration>()
                .HasMany(e => e.reg_Data)
                .WithRequired(e => e.reg__Registration)
                .HasForeignKey(e => e.rd_RegId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<reg__Registration>()
                .HasMany(e => e.reg_JournalDeals)
                .WithRequired(e => e.reg__Registration)
                .HasForeignKey(e => e.rjd_RegId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<reg__Registration>()
                .HasMany(e => e.reg_Site)
                .WithRequired(e => e.reg__Registration)
                .HasForeignKey(e => e.rs_RegId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<reg__Registration>()
                .HasMany(e => e.st_Category)
                .WithOptional(e => e.reg__Registration)
                .HasForeignKey(e => e.cat_RegId);

            modelBuilder.Entity<reg__Registration>()
                .HasMany(e => e.tr__Transaction)
                .WithRequired(e => e.reg__Registration)
                .HasForeignKey(e => e.tr_RegId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<reg_Data>()
                .Property(e => e.rd_vmoney)
                .HasPrecision(19, 4);

            modelBuilder.Entity<reg_Data>()
                .Property(e => e.rd_vbinary)
                .IsFixedLength();

            modelBuilder.Entity<reg_Site>()
                .HasMany(e => e.ctx_UserSite)
                .WithRequired(e => e.reg_Site)
                .HasForeignKey(e => e.cus_RegSiteId);

            modelBuilder.Entity<reg_Site>()
                .HasMany(e => e.pc__Package)
                .WithOptional(e => e.reg_Site)
                .HasForeignKey(e => e.pc_SiteId);

            modelBuilder.Entity<reg_Site>()
                .HasMany(e => e.tr__Transaction)
                .WithRequired(e => e.reg_Site)
                .HasForeignKey(e => e.tr_SiteId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_AuctionGroup>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.sl_AuctionGroup)
                .HasForeignKey(e => e.au_GroupId);

            modelBuilder.Entity<sl_AuctionTemplateGroup>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.sl_AuctionTemplateGroup)
                .HasForeignKey(e => e.au_TemplGroupId);

            modelBuilder.Entity<sl_Currency>()
                .Property(e => e.cy_Symbol)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<sl_Currency>()
                .Property(e => e.cy_Multiplier)
                .HasPrecision(19, 4);

            modelBuilder.Entity<sl_CurrencyBank>()
                .HasMany(e => e.it_ItemSite)
                .WithOptional(e => e.sl_CurrencyBank)
                .HasForeignKey(e => e.is_AC_ExchangeRateBankId);

            modelBuilder.Entity<sl_CurrencyBank>()
                .HasMany(e => e.sl_CurrencyExchangeRates)
                .WithRequired(e => e.sl_CurrencyBank)
                .HasForeignKey(e => e.er_BankId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_CurrencyExchangeRates>()
                .HasMany(e => e.sl_CurrencyExchangeRatesEntry)
                .WithRequired(e => e.sl_CurrencyExchangeRates)
                .HasForeignKey(e => e.ere_ExchangeRatesId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_CurrencyExchangeRatesEntry>()
                .Property(e => e.ere_CurrencyId)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<sl_CurrencyExchangeRatesEntry>()
                .Property(e => e.ere_Purchase)
                .HasPrecision(19, 4);

            modelBuilder.Entity<sl_CurrencyExchangeRatesEntry>()
                .Property(e => e.ere_Sell)
                .HasPrecision(19, 4);

            modelBuilder.Entity<sl_CurrencyExchangeRatesEntry>()
                .Property(e => e.ere_Avg)
                .HasPrecision(19, 4);

            modelBuilder.Entity<sl_CustomAttributes>()
                .HasMany(e => e.it_CustomAttributes)
                .WithRequired(e => e.sl_CustomAttributes)
                .HasForeignKey(e => e.ia_AttributeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_CustomerGroup>()
                .HasMany(e => e.cs__Customer)
                .WithOptional(e => e.sl_CustomerGroup)
                .HasForeignKey(e => e.cs_GroupId);

            modelBuilder.Entity<sl_CustProperty>()
                .HasMany(e => e.cs_CustomerProp)
                .WithRequired(e => e.sl_CustProperty)
                .HasForeignKey(e => e.cp_PropId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_Deliverer>()
                .HasMany(e => e.pc__Package)
                .WithOptional(e => e.sl_Deliverer)
                .HasForeignKey(e => e.pc_DelivererId);

            modelBuilder.Entity<sl_Deliverer>()
                .HasMany(e => e.pc_SendingList)
                .WithOptional(e => e.sl_Deliverer)
                .HasForeignKey(e => e.pl_DelivererId);

            modelBuilder.Entity<sl_Deliverer>()
                .HasMany(e => e.sl_Delivery)
                .WithRequired(e => e.sl_Deliverer)
                .HasForeignKey(e => e.dm_DelivererId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_Deliverer>()
                .HasMany(e => e.tr__Transaction)
                .WithOptional(e => e.sl_Deliverer)
                .HasForeignKey(e => e.tr_DelivererId);

            modelBuilder.Entity<sl_Deliverer>()
                .HasMany(e => e.tr_Default)
                .WithOptional(e => e.sl_Deliverer)
                .HasForeignKey(e => e.td_DelivererId);

            modelBuilder.Entity<sl_Deliverer>()
                .HasMany(e => e.tr_Default1)
                .WithOptional(e => e.sl_Deliverer1)
                .HasForeignKey(e => e.td_DelivererIdPOD);

            modelBuilder.Entity<sl_Delivery>()
                .HasMany(e => e.ds_DeliveryToShippingMap)
                .WithRequired(e => e.sl_Delivery)
                .HasForeignKey(e => e.ds_DeliveryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_Delivery>()
                .HasMany(e => e.pc__Package)
                .WithOptional(e => e.sl_Delivery)
                .HasForeignKey(e => e.pc_DeliveryId);

            modelBuilder.Entity<sl_Delivery>()
                .HasMany(e => e.pc__Package1)
                .WithOptional(e => e.sl_Delivery1)
                .HasForeignKey(e => e.pc_DeliveryId);

            modelBuilder.Entity<sl_Delivery>()
                .HasMany(e => e.pf_Delivery)
                .WithRequired(e => e.sl_Delivery)
                .HasForeignKey(e => e.pd_DeliveryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_Delivery>()
                .HasMany(e => e.tr_Default)
                .WithOptional(e => e.sl_Delivery)
                .HasForeignKey(e => e.td_DeliveryId);

            modelBuilder.Entity<sl_Delivery>()
                .HasMany(e => e.tr__Transaction)
                .WithOptional(e => e.sl_Delivery)
                .HasForeignKey(e => e.tr_DeliveryId);

            modelBuilder.Entity<sl_Delivery>()
                .HasMany(e => e.tr__Transaction1)
                .WithOptional(e => e.sl_Delivery1)
                .HasForeignKey(e => e.tr_DeliveryId);

            modelBuilder.Entity<sl_Delivery>()
                .HasMany(e => e.tr_Default1)
                .WithOptional(e => e.sl_Delivery1)
                .HasForeignKey(e => e.td_DeliveryIdPOD);

            modelBuilder.Entity<sl_EmailGroup>()
                .Property(e => e.eg_Name)
                .IsUnicode(false);

            modelBuilder.Entity<sl_EmailGroup>()
                .HasMany(e => e.em__Email)
                .WithOptional(e => e.sl_EmailGroup)
                .HasForeignKey(e => e.em_GroupId);

            modelBuilder.Entity<sl_EmailGroup>()
                .HasMany(e => e.em_Rule)
                .WithOptional(e => e.sl_EmailGroup)
                .HasForeignKey(e => e.er_GroupId);

            modelBuilder.Entity<sl_InstantMessenger>()
                .HasMany(e => e.cs_CustIM)
                .WithRequired(e => e.sl_InstantMessenger)
                .HasForeignKey(e => e.cm_IMTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_ItemGroup>()
                .HasMany(e => e.it__Item)
                .WithOptional(e => e.sl_ItemGroup)
                .HasForeignKey(e => e.it_GroupId);

            modelBuilder.Entity<sl_ItemProperty>()
                .HasMany(e => e.it_ItemProp)
                .WithRequired(e => e.sl_ItemProperty)
                .HasForeignKey(e => e.ip_PropertyId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_Language>()
                .HasMany(e => e.tp__HtmlTemplate)
                .WithRequired(e => e.sl_Language)
                .HasForeignKey(e => e.tp_LanguageId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_PackageSynchronizationParams>()
                .Property(e => e.sp_vmoney)
                .HasPrecision(19, 4);

            modelBuilder.Entity<sl_PackageSynchronizationParams>()
                .Property(e => e.sp_vbinary)
                .IsFixedLength();

            modelBuilder.Entity<sl_PricingList>()
                .HasMany(e => e.pl_PricingListData)
                .WithRequired(e => e.sl_PricingList)
                .HasForeignKey(e => e.pd_PricingListId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_SendingNumbersPool>()
                .Property(e => e.np_SendingPlace)
                .IsFixedLength();

            modelBuilder.Entity<sl_SendingNumbersPool>()
                .HasMany(e => e.pc__Package)
                .WithOptional(e => e.sl_SendingNumbersPool)
                .HasForeignKey(e => e.pc_SendingNumberPoolId);

            modelBuilder.Entity<sl_SendingNumbersPool>()
                .HasMany(e => e.rn_ReturnedNumber)
                .WithRequired(e => e.sl_SendingNumbersPool)
                .HasForeignKey(e => e.rn_SendingNumbersPoolId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_SendingNumbersPoolType>()
                .HasMany(e => e.sl_Delivery)
                .WithOptional(e => e.sl_SendingNumbersPoolType)
                .HasForeignKey(e => e.dm_SendingNumbersPoolTypeId);

            modelBuilder.Entity<sl_SendingNumbersPoolType>()
                .HasMany(e => e.sl_SendingNumbersPool)
                .WithRequired(e => e.sl_SendingNumbersPoolType)
                .HasForeignKey(e => e.np_Type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_ServiceProvider>()
                .HasMany(e => e.cs__Customer)
                .WithOptional(e => e.sl_ServiceProvider)
                .HasForeignKey(e => e.cs_SvcProvId);

            modelBuilder.Entity<sl_TransProperty>()
                .HasMany(e => e.au_TransactionProperty)
                .WithRequired(e => e.sl_TransProperty)
                .HasForeignKey(e => e.atp_TransactionPropertyId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_TransProperty>()
                .HasMany(e => e.tr_Default)
                .WithOptional(e => e.sl_TransProperty)
                .HasForeignKey(e => e.td_ManualTransactionProperty1);

            modelBuilder.Entity<sl_TransProperty>()
                .HasMany(e => e.tr_Default1)
                .WithOptional(e => e.sl_TransProperty1)
                .HasForeignKey(e => e.td_ManualTransactionProperty2);

            modelBuilder.Entity<sl_TransProperty>()
                .HasMany(e => e.tr_Default2)
                .WithOptional(e => e.sl_TransProperty2)
                .HasForeignKey(e => e.td_ManualTransactionProperty3);

            modelBuilder.Entity<sl_TransProperty>()
                .HasMany(e => e.tr_Property)
                .WithRequired(e => e.sl_TransProperty)
                .HasForeignKey(e => e.tp_PropId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.sl_User)
                .HasForeignKey(e => e.au_UserId);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.cs__Customer)
                .WithOptional(e => e.sl_User)
                .HasForeignKey(e => e.cs_AnonimisedByUser);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.ctx_UserSite)
                .WithRequired(e => e.sl_User)
                .HasForeignKey(e => e.cus_UserId);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.em_Account)
                .WithRequired(e => e.sl_User)
                .HasForeignKey(e => e.ea_UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.ep_UserMapping)
                .WithOptional(e => e.sl_User)
                .HasForeignKey(e => e.um_FhUserId);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.jb__Job)
                .WithRequired(e => e.sl_User)
                .HasForeignKey(e => e.lg_UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.nt_Note)
                .WithOptional(e => e.sl_User)
                .HasForeignKey(e => e.ne_UserId);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.pc__Package)
                .WithOptional(e => e.sl_User)
                .HasForeignKey(e => e.pc_OwnerId);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.pd_AutoSyncParams)
                .WithOptional(e => e.sl_User)
                .HasForeignKey(e => e.pa_ServerUserId);

            modelBuilder.Entity<sl_User>()
                .HasMany(e => e.pd_UserPermission)
                .WithRequired(e => e.sl_User)
                .HasForeignKey(e => e.up_UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st__Site>()
                .Property(e => e.st_Currency)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<st__Site>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.st__Site)
                .HasForeignKey(e => e.au_SiteId);

            modelBuilder.Entity<st__Site>()
                .HasMany(e => e.it_ItemSite)
                .WithOptional(e => e.st__Site)
                .HasForeignKey(e => e.is_SiteId);

            modelBuilder.Entity<st__Site>()
                .HasMany(e => e.reg_JournalDeals)
                .WithRequired(e => e.st__Site)
                .HasForeignKey(e => e.rjd_SiteId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st__Site>()
                .HasMany(e => e.reg_Site)
                .WithRequired(e => e.st__Site)
                .HasForeignKey(e => e.rs_SiteId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st__Site>()
                .HasMany(e => e.st_Category)
                .WithRequired(e => e.st__Site)
                .HasForeignKey(e => e.cat_SiteId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st__Site>()
                .HasMany(e => e.st_SiteData)
                .WithRequired(e => e.st__Site)
                .HasForeignKey(e => e.sd_SiteId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st_AuctionType>()
                .HasMany(e => e.au__Auction)
                .WithRequired(e => e.st_AuctionType)
                .HasForeignKey(e => e.au_TypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st_Category>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.st_Category)
                .HasForeignKey(e => e.au_ShopCatId);

            modelBuilder.Entity<st_CategorySpecific>()
                .HasMany(e => e.st_CategorySpecificDict)
                .WithRequired(e => e.st_CategorySpecific)
                .HasForeignKey(e => e.csd_CatSpecId);

            modelBuilder.Entity<st_Country>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.st_Country)
                .HasForeignKey(e => e.au_CountryId);

            modelBuilder.Entity<st_Duration>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.st_Duration)
                .HasForeignKey(e => e.au_DurationId);

            modelBuilder.Entity<st_Enhancement>()
                .HasMany(e => e.au_Enhancement)
                .WithRequired(e => e.st_Enhancement)
                .HasForeignKey(e => e.ae_OptionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st_Insurance>()
                .HasMany(e => e.au_Ebay)
                .WithOptional(e => e.st_Insurance)
                .HasForeignKey(e => e.aeb_InsuranceDomId);

            modelBuilder.Entity<st_Insurance>()
                .HasMany(e => e.au_Ebay1)
                .WithOptional(e => e.st_Insurance1)
                .HasForeignKey(e => e.aeb_InsuranceIntlId);

            modelBuilder.Entity<st_Payment>()
                .HasMany(e => e.au_Payment)
                .WithRequired(e => e.st_Payment)
                .HasForeignKey(e => e.ap_OptionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st_Region>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.st_Region)
                .HasForeignKey(e => e.au_RegionId);

            modelBuilder.Entity<st_Shipping>()
                .HasMany(e => e.au_Shipping)
                .WithRequired(e => e.st_Shipping)
                .HasForeignKey(e => e.as_OptionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st_Shipping>()
                .HasMany(e => e.pl_PricingListData)
                .WithRequired(e => e.st_Shipping)
                .HasForeignKey(e => e.pd_OptionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<st_SiteData>()
                .Property(e => e.sd_vmoney)
                .HasPrecision(19, 4);

            modelBuilder.Entity<st_SiteData>()
                .Property(e => e.sd_vbinary)
                .IsFixedLength();

            modelBuilder.Entity<tp__HtmlTemplate>()
                .HasMany(e => e.au__Auction)
                .WithOptional(e => e.tp__HtmlTemplate)
                .HasForeignKey(e => e.au_HtmlTemplateId);

            modelBuilder.Entity<tp__HtmlTemplate>()
                .HasMany(e => e.tp_Image)
                .WithRequired(e => e.tp__HtmlTemplate)
                .HasForeignKey(e => e.ti_HtmlTemplateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_DeliveryCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_Payment)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_Remittance)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_DiscountPercent)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_DiscountAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_BaseCurrency)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_PaymentCurrency)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_ExchangeRate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr__Transaction>()
                .Property(e => e.tr_Weight)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.adr__Address)
                .WithOptional(e => e.tr__Transaction)
                .HasForeignKey(e => e.adr_TransId);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.em__Email)
                .WithOptional(e => e.tr__Transaction)
                .HasForeignKey(e => e.em_TransId);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.em__Email1)
                .WithOptional(e => e.tr__Transaction1)
                .HasForeignKey(e => e.em_PrevTransId);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.pb_PostBuyFormTransaction)
                .WithRequired(e => e.tr__Transaction)
                .HasForeignKey(e => e.pt_TransactionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.pc__Package)
                .WithOptional(e => e.tr__Transaction)
                .HasForeignKey(e => e.pc_TransId);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.tr_Comment)
                .WithRequired(e => e.tr__Transaction)
                .HasForeignKey(e => e.tc_TransId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.tr_Group1)
                .WithRequired(e => e.tr__Transaction)
                .HasForeignKey(e => e.tg_TransId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.tr_Group2)
                .WithRequired(e => e.tr__Transaction1)
                .HasForeignKey(e => e.tg_PrimaryTransId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.tr_Item)
                .WithOptional(e => e.tr__Transaction)
                .HasForeignKey(e => e.tt_OrigTransId);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.tr_Property)
                .WithRequired(e => e.tr__Transaction)
                .HasForeignKey(e => e.tp_TransId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tr__Transaction>()
                .HasMany(e => e.tr_Item1)
                .WithRequired(e => e.tr__Transaction1)
                .HasForeignKey(e => e.tt_TransId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tr_Default>()
                .Property(e => e.td_OwnDefaultCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr_Item>()
                .Property(e => e.tt_Price)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr_Item>()
                .Property(e => e.tt_UnitWeight)
                .HasPrecision(19, 4);

            modelBuilder.Entity<tr_Item>()
                .HasMany(e => e.pc_Item)
                .WithOptional(e => e.tr_Item)
                .HasForeignKey(e => e.pt_TransItemId);

            modelBuilder.Entity<up_UserParams>()
                .Property(e => e.up_vmoney)
                .HasPrecision(19, 4);

            modelBuilder.Entity<up_UserParams>()
                .Property(e => e.up_vbinary)
                .IsFixedLength();

            modelBuilder.Entity<xcs_Szum>()
                .Property(e => e.szcs_Item)
                .IsUnicode(false);

            modelBuilder.Entity<xit_Szum>()
                .Property(e => e.szit_Item)
                .IsUnicode(false);

            modelBuilder.Entity<au_EbayReturnPolicy>()
                .Property(e => e.aurp_EAN)
                .IsUnicode(false);

            modelBuilder.Entity<ep_RegData>()
                .Property(e => e.erd_vmoney)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ep_RegData>()
                .Property(e => e.erd_vbinary)
                .IsFixedLength();

            modelBuilder.Entity<gr__KonfiguracjaEx>()
                .Property(e => e.cge_Section)
                .IsUnicode(false);

            modelBuilder.Entity<gr__KonfiguracjaEx>()
                .Property(e => e.cge_Version)
                .IsUnicode(false);

            modelBuilder.Entity<gr__KonfiguracjaEx>()
                .Property(e => e.cge_Sql)
                .IsUnicode(false);

            modelBuilder.Entity<gr__KonfiguracjaEx>()
                .Property(e => e.cge_LastUpdateVersion)
                .IsUnicode(false);

            modelBuilder.Entity<xcs_Ewid>()
                .Property(e => e.csx_Entry)
                .IsUnicode(false);

            modelBuilder.Entity<xit_Ewid>()
                .Property(e => e.itx_Entry)
                .IsUnicode(false);
        }
    }
}
