﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnection.Factories
{
    public class ProductFactory : BaseFactory
    {
        public List<string> GetAllProductNamesWithCode()
        {
            Context = new SelloContext();
            var toReturn = new List<string>();
            foreach (var product in GetAllProducts())
            {
                toReturn.Add($"{product.it_Name} ({product.it_Symbol})");
            }

            return toReturn;
        }

        public List<it__Item> GetAllProducts()
        {
            Context = new SelloContext();
            var toReturn = new List<it__Item>();
            using (Context)
            {
                toReturn = Context.it__Item.ToList();
            }
            return toReturn;
        }

        public int GetProductIdBySymbol(string symbol)
        {
            Context = new SelloContext();
            it__Item toReturn;
            using (SelloContext context2 = new SelloContext())
            {
                toReturn = context2.it__Item.ToList().Where(p => p.it_Symbol == symbol).First();
            }

            return toReturn.it_Id;
        }
    }
}
