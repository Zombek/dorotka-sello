﻿using DatabaseConnection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseConnection.Factories
{
    public class PackageFactory : BaseFactory
    {
        public List<pc__Package> GetPackagesForDeliverer(int delivereId, DateTime sendingDate)
        {
            var packages = new List<pc__Package>();
            Context = new SelloContext();
            using (Context)
            {
                packages = Context.pc__Package.Where(pc => pc.pc_DelivererId == delivereId).ToList();
            }

            var toReturn = new List<pc__Package>();
            foreach (var item in packages)
            {
                if (CompareDates(item.pc_SendingDate, sendingDate))
                {
                    toReturn.Add(item);
                }
            }

            return toReturn;
        }
    }
}
