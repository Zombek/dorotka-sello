﻿using DatabaseConnection;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseConnection.Factories
{
    public class DelivererFactory : BaseFactory
    {
        public DelivererFactory()
        {

        }

        public List<sl_Deliverer> GetAllDeliverers()
        {
            Context = new SelloContext();
            using (Context)
            {
                return Context.sl_Deliverer.ToList();
            }
        }

        public sl_Deliverer Get_DelivererByName(string name)
        {
            return GetAllDeliverers().Where(del => del.dr_Name == name).Single();
        }

        public sl_Deliverer Get_DelivererById(int id)
        {
            return GetAllDeliverers().Where(del => del.dr_Id == id).Single();
        }
    }
}
