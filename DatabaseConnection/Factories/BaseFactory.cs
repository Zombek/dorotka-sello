﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnection.Factories
{
    public abstract class BaseFactory
    {
        protected SelloContext Context;

        protected bool CompareDates(DateTime? dateOne, DateTime dateTwo)
        {
            bool isEqual = false;
            var dateTimeToCompare = dateOne.GetValueOrDefault();

            if (dateTimeToCompare.Year == dateTwo.Year &&
                dateTimeToCompare.Month == dateTwo.Month &&
                dateTimeToCompare.Day == dateTwo.Day
                )
            {
                isEqual = true;
            }

            return isEqual;
        }
    }
}
