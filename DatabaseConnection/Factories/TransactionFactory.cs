﻿using DatabaseConnection.Models.OwnModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DatabaseConnection.Factories
{
    public class TransactionFactory : BaseFactory
    {
        public List<tr__Transaction> GetAllDeliverers()
        {
            Context = new SelloContext();
            using (Context)
            {
                return Context.tr__Transaction.ToList();
            }
        }

        public List<CustomerTransactionInfo> GetTransactionsWithCustomerAddres(int itemId, int delivererId, DateTime dateTime)
        {
            Context = new SelloContext();
            using (Context)
            {

                var transactionInfo = (from transactions in Context.tr__Transaction
                                       join adresses in Context.adr__Address on transactions.tr_Id equals adresses.tr__Transaction.tr_Id
                                       join package in Context.pc__Package on transactions.tr_Id equals package.pc_TransId
                                       join items in Context.tr_Item on transactions.tr_Id equals items.tt_TransId
                                       where adresses.adr_Type == 1
                                       && items.tt_ItemId == itemId
                                       && package.pc_CreationDate.Year == dateTime.Year
                                       && package.pc_CreationDate.Month == dateTime.Month
                                       && package.pc_CreationDate.Day == dateTime.Day
                                       && package.pc_DelivererId == delivererId
                                       select new
                                       {
                                           payOnDelivery = package.pc_PayOnDelivery,
                                           isCompany = adresses.adr_NIP,
                                           customerName = adresses.adr_Name,
                                           companyName = adresses.adr_Company,
                                           email = adresses.adr_Email,
                                           street = adresses.adr_Address1 + " " + adresses.adr_Address2,
                                           zipCode = adresses.adr_ZipCode,
                                           city = adresses.adr_City,
                                           phone = adresses.adr_PhoneNumber,
                                           referenceNumber = package.pc_SymbolIndex + "/" + package.pc_SymbolYear,
                                           payment = transactions.tr_Payment,
                                           paymentOnDelivery = package.pc_Charge

                            }).ToList();

                List<CustomerTransactionInfo> toReturn = new List<CustomerTransactionInfo>();
                foreach (var info in transactionInfo)
                {
                    toReturn.Add(new CustomerTransactionInfo(info.payOnDelivery,
                        info.isCompany,
                        info.customerName,
                        info.companyName,
                        info.email,
                        info.street,
                        info.zipCode,
                        info.city,
                        info.phone,
                        info.referenceNumber,
                        info.payment,
                        info.paymentOnDelivery));
                }

                return toReturn;
            }
        }
    }
}
