﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationManagement
{
    public static class FileConfigurator
    {
        public static string GetLocationOfStandardFiles()
        {
            var location = GetValueFromSettings("CsvLocation");
            //The specified location in app.config or default MyDocuments on the PC
            return string.IsNullOrEmpty(location) || location.Length == 0 ? System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) : location;
        }

        public static string GetValueFromSettings(string setting)
        {
            return ConfigurationManager.AppSettings.Get(setting);
        }

        public static void SaveConfigValue(string key, string value)
        {
            try
            {
                Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                configuration.AppSettings.Settings[key].Value = value;
                configuration.Save();

                ConfigurationManager.RefreshSection("appSettings");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static CompanyInfo GetCompanyInfo()
        {
            var info = new CompanyInfo();
            info.OwnerName = GetValueFromSettings("OwnerName");
            info.OwnerSurname = GetValueFromSettings("OwnerSurname");
            info.CompanyName = GetValueFromSettings("CompanyName");
            info.CompanyEmail = GetValueFromSettings("CompanyEmail");
            info.Street = GetValueFromSettings("Street");
            info.Postcode = GetValueFromSettings("Postcode");
            info.City = GetValueFromSettings("City");
            info.Phone = GetValueFromSettings("Phone");
            info.BankAccountNumber = GetValueFromSettings("BankAccountNumber");

            return info;
        }

        public static bool SetCompanyInfo(CompanyInfo company)
        {
            try
            {
                SaveConfigValue("OwnerName", company.OwnerName);
                SaveConfigValue("OwnerSurname", company.OwnerSurname);
                SaveConfigValue("CompanyName", company.CompanyName);
                SaveConfigValue("CompanyEmail",company.CompanyEmail);
                SaveConfigValue("Street", company.Street);
                SaveConfigValue("Postcode", company.Postcode);
                SaveConfigValue("City", company.City);
                SaveConfigValue("Phone", company.Phone);
                SaveConfigValue("BankAccountNumber", company.BankAccountNumber);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
