﻿namespace ConfigurationManagement
{
    public class CompanyInfo
    {
        public string OwnerName { get; set; }
        public string OwnerSurname { get; set; }
        public string CompanyName { get; set; }
        public string CompanyEmail{ get; set; }
        public string Street { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string BankAccountNumber { get; set; }
    }
}