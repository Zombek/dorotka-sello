﻿namespace Dorotka___Sello.Controls
{
    partial class ConfigControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OwnerNameBox = new System.Windows.Forms.TextBox();
            this.OwnerSurnameBox = new System.Windows.Forms.TextBox();
            this.CompanyBox = new System.Windows.Forms.TextBox();
            this.ComapnyEmailBox = new System.Windows.Forms.TextBox();
            this.PostCodeBox = new System.Windows.Forms.TextBox();
            this.CityBox = new System.Windows.Forms.TextBox();
            this.StreetBox = new System.Windows.Forms.TextBox();
            this.BankAccountNumberBox = new System.Windows.Forms.TextBox();
            this.PhoneBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.saveButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.successInfoLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OwnerNameBox
            // 
            this.OwnerNameBox.Location = new System.Drawing.Point(3, 3);
            this.OwnerNameBox.Name = "OwnerNameBox";
            this.OwnerNameBox.Size = new System.Drawing.Size(270, 20);
            this.OwnerNameBox.TabIndex = 0;
            this.OwnerNameBox.TextChanged += new System.EventHandler(this.OwnerNameBox_TextChanged);
            // 
            // OwnerSurnameBox
            // 
            this.OwnerSurnameBox.Location = new System.Drawing.Point(3, 29);
            this.OwnerSurnameBox.Name = "OwnerSurnameBox";
            this.OwnerSurnameBox.Size = new System.Drawing.Size(270, 20);
            this.OwnerSurnameBox.TabIndex = 1;
            this.OwnerSurnameBox.TextChanged += new System.EventHandler(this.OwnerSurnameBox_TextChanged);
            // 
            // CompanyBox
            // 
            this.CompanyBox.Location = new System.Drawing.Point(3, 55);
            this.CompanyBox.Name = "CompanyBox";
            this.CompanyBox.Size = new System.Drawing.Size(270, 20);
            this.CompanyBox.TabIndex = 2;
            this.CompanyBox.TextChanged += new System.EventHandler(this.CompanyBox_TextChanged);
            // 
            // ComapnyEmailBox
            // 
            this.ComapnyEmailBox.Location = new System.Drawing.Point(3, 81);
            this.ComapnyEmailBox.Name = "ComapnyEmailBox";
            this.ComapnyEmailBox.Size = new System.Drawing.Size(270, 20);
            this.ComapnyEmailBox.TabIndex = 3;
            this.ComapnyEmailBox.TextChanged += new System.EventHandler(this.ComapnyEmailBox_TextChanged);
            // 
            // PostCodeBox
            // 
            this.PostCodeBox.Location = new System.Drawing.Point(3, 185);
            this.PostCodeBox.Name = "PostCodeBox";
            this.PostCodeBox.Size = new System.Drawing.Size(270, 20);
            this.PostCodeBox.TabIndex = 4;
            this.PostCodeBox.TextChanged += new System.EventHandler(this.PostCodeBox_TextChanged);
            // 
            // CityBox
            // 
            this.CityBox.Location = new System.Drawing.Point(3, 159);
            this.CityBox.Name = "CityBox";
            this.CityBox.Size = new System.Drawing.Size(270, 20);
            this.CityBox.TabIndex = 5;
            this.CityBox.TextChanged += new System.EventHandler(this.CityBox_TextChanged);
            // 
            // StreetBox
            // 
            this.StreetBox.Location = new System.Drawing.Point(3, 133);
            this.StreetBox.Name = "StreetBox";
            this.StreetBox.Size = new System.Drawing.Size(270, 20);
            this.StreetBox.TabIndex = 6;
            this.StreetBox.TextChanged += new System.EventHandler(this.StreetBox_TextChanged);
            // 
            // BankAccountNumberBox
            // 
            this.BankAccountNumberBox.Location = new System.Drawing.Point(3, 269);
            this.BankAccountNumberBox.Name = "BankAccountNumberBox";
            this.BankAccountNumberBox.Size = new System.Drawing.Size(270, 20);
            this.BankAccountNumberBox.TabIndex = 7;
            this.BankAccountNumberBox.TextChanged += new System.EventHandler(this.BankAccountNumberBox_TextChanged);
            // 
            // PhoneBox
            // 
            this.PhoneBox.Location = new System.Drawing.Point(3, 243);
            this.PhoneBox.Name = "PhoneBox";
            this.PhoneBox.Size = new System.Drawing.Size(270, 20);
            this.PhoneBox.TabIndex = 8;
            this.PhoneBox.TextChanged += new System.EventHandler(this.PhoneBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(288, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Imię właściciela";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(288, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Nazwisko właściciela";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(288, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nazwa firmy";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(288, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Email firmy";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(288, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Ulica";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(288, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Miasto";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(288, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Kod pocztowy";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(288, 272);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Numer konta bankowego";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(288, 246);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Numer telefonu";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(620, 310);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 18;
            this.saveButton.Text = "Zapisz";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(526, 310);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 19;
            this.exitButton.Text = "Wyjdź";
            this.exitButton.UseVisualStyleBackColor = true;
            // 
            // successInfoLabel
            // 
            this.successInfoLabel.AutoSize = true;
            this.successInfoLabel.Location = new System.Drawing.Point(34, 310);
            this.successInfoLabel.Name = "successInfoLabel";
            this.successInfoLabel.Size = new System.Drawing.Size(0, 13);
            this.successInfoLabel.TabIndex = 20;
            // 
            // ConfigControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.successInfoLabel);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PhoneBox);
            this.Controls.Add(this.BankAccountNumberBox);
            this.Controls.Add(this.StreetBox);
            this.Controls.Add(this.CityBox);
            this.Controls.Add(this.PostCodeBox);
            this.Controls.Add(this.ComapnyEmailBox);
            this.Controls.Add(this.CompanyBox);
            this.Controls.Add(this.OwnerSurnameBox);
            this.Controls.Add(this.OwnerNameBox);
            this.Name = "ConfigControl";
            this.Size = new System.Drawing.Size(708, 364);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox OwnerNameBox;
        private System.Windows.Forms.TextBox OwnerSurnameBox;
        private System.Windows.Forms.TextBox CompanyBox;
        private System.Windows.Forms.TextBox ComapnyEmailBox;
        private System.Windows.Forms.TextBox PostCodeBox;
        private System.Windows.Forms.TextBox CityBox;
        private System.Windows.Forms.TextBox StreetBox;
        private System.Windows.Forms.TextBox BankAccountNumberBox;
        private System.Windows.Forms.TextBox PhoneBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label successInfoLabel;
    }
}
