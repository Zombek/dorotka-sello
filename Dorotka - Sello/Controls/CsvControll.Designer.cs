﻿namespace Dorotka___Sello.Controls
{
    partial class CsvControll
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.delivererBox = new System.Windows.Forms.ComboBox();
            this.csvButton = new System.Windows.Forms.Button();
            this.entityCommand1 = new System.Data.Entity.Core.EntityClient.EntityCommand();
            this.productBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(29, 154);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker.TabIndex = 0;
            // 
            // delivererBox
            // 
            this.delivererBox.FormattingEnabled = true;
            this.delivererBox.Location = new System.Drawing.Point(29, 69);
            this.delivererBox.Name = "delivererBox";
            this.delivererBox.Size = new System.Drawing.Size(200, 21);
            this.delivererBox.TabIndex = 1;
            // 
            // csvButton
            // 
            this.csvButton.Location = new System.Drawing.Point(29, 200);
            this.csvButton.Name = "csvButton";
            this.csvButton.Size = new System.Drawing.Size(200, 23);
            this.csvButton.TabIndex = 2;
            this.csvButton.Text = "Generuj CSV";
            this.csvButton.UseVisualStyleBackColor = true;
            this.csvButton.Click += new System.EventHandler(this.csvButton_Click);
            // 
            // entityCommand1
            // 
            this.entityCommand1.CommandTimeout = 0;
            this.entityCommand1.CommandTree = null;
            this.entityCommand1.Connection = null;
            this.entityCommand1.EnablePlanCaching = true;
            this.entityCommand1.Transaction = null;
            // 
            // productBox
            // 
            this.productBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.productBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.productBox.FormattingEnabled = true;
            this.productBox.Location = new System.Drawing.Point(29, 110);
            this.productBox.Name = "productBox";
            this.productBox.Size = new System.Drawing.Size(200, 21);
            this.productBox.TabIndex = 3;
            // 
            // CsvControll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.productBox);
            this.Controls.Add(this.csvButton);
            this.Controls.Add(this.delivererBox);
            this.Controls.Add(this.dateTimePicker);
            this.Name = "CsvControll";
            this.Size = new System.Drawing.Size(443, 306);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.ComboBox delivererBox;
        private System.Windows.Forms.Button csvButton;
        private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand1;
        private System.Windows.Forms.ComboBox productBox;
    }
}
