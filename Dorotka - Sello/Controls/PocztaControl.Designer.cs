﻿namespace Dorotka___Sello.Controls
{
    partial class PocztaControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.entityCommand1 = new System.Data.Entity.Core.EntityClient.EntityCommand();
            this.delivererBox = new System.Windows.Forms.ComboBox();
            this.checkPackages = new System.Windows.Forms.Button();
            this.informationLabel = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(25, 25);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker.TabIndex = 0;
            // 
            // entityCommand1
            // 
            this.entityCommand1.CommandTimeout = 0;
            this.entityCommand1.CommandTree = null;
            this.entityCommand1.Connection = null;
            this.entityCommand1.EnablePlanCaching = true;
            this.entityCommand1.Transaction = null;
            // 
            // delivererBox
            // 
            this.delivererBox.FormattingEnabled = true;
            this.delivererBox.Location = new System.Drawing.Point(25, 51);
            this.delivererBox.Name = "delivererBox";
            this.delivererBox.Size = new System.Drawing.Size(200, 21);
            this.delivererBox.TabIndex = 1;
            this.delivererBox.SelectedIndexChanged += new System.EventHandler(this.delivererBox_SelectedIndexChanged);
            // 
            // checkPackages
            // 
            this.checkPackages.Location = new System.Drawing.Point(25, 88);
            this.checkPackages.Name = "checkPackages";
            this.checkPackages.Size = new System.Drawing.Size(200, 22);
            this.checkPackages.TabIndex = 3;
            this.checkPackages.Text = "Sprawdź przesyłki";
            this.checkPackages.UseVisualStyleBackColor = true;
            this.checkPackages.Click += new System.EventHandler(this.checkPackages_Click);
            // 
            // informationLabel
            // 
            this.informationLabel.AutoSize = true;
            this.informationLabel.Location = new System.Drawing.Point(245, 59);
            this.informationLabel.Name = "informationLabel";
            this.informationLabel.Size = new System.Drawing.Size(35, 13);
            this.informationLabel.TabIndex = 5;
            this.informationLabel.Text = "label2";
            // 
            // PocztaControl
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.informationLabel);
            this.Controls.Add(this.checkPackages);
            this.Controls.Add(this.delivererBox);
            this.Controls.Add(this.dateTimePicker);
            this.Name = "PocztaControl";
            this.Size = new System.Drawing.Size(720, 467);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand1;
        private System.Windows.Forms.ComboBox delivererBox;
        private System.Windows.Forms.Button checkPackages;
        private System.Windows.Forms.Label informationLabel;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}
