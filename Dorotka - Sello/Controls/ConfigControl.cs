﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ConfigurationManagement;

namespace Dorotka___Sello.Controls
{
    public partial class ConfigControl : UserControl
    {
        CompanyInfo OriginalcompanyInfo;
        public ConfigControl()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            SetupInfoForCompany();
            base.OnLoad(e);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            CompanyInfo company = new CompanyInfo();
            company.OwnerName = OwnerNameBox.Text;
            company.OwnerSurname = OwnerSurnameBox.Text;
            company.CompanyName = CompanyBox.Text;
            company.CompanyEmail = ComapnyEmailBox.Text;
            company.Street = StreetBox.Text;
            company.City = CityBox.Text;
            company.Postcode = PostCodeBox.Text;
            company.Phone = PhoneBox.Text;
            company.BankAccountNumber = BankAccountNumberBox.Text;

            bool success = FileConfigurator.SetCompanyInfo(company);

            if (success)
            {
                successInfoLabel.Text = "Zmiany zostały pomyślnie zapisane";
            }
            else
            {
                successInfoLabel.Text = "Wystąpił błąd - wprowadź zmiany ręcznie w pliku XML";
            }
        }

        private void SetupInfoForCompany()
        {
            OriginalcompanyInfo = FileConfigurator.GetCompanyInfo();

            OwnerNameBox.Text = OriginalcompanyInfo.OwnerName;
            OwnerSurnameBox.Text = OriginalcompanyInfo.OwnerSurname;
            CompanyBox.Text = OriginalcompanyInfo.CompanyName;
            ComapnyEmailBox.Text = OriginalcompanyInfo.CompanyEmail;
            StreetBox.Text = OriginalcompanyInfo.Street;
            CityBox.Text = OriginalcompanyInfo.City;
            PostCodeBox.Text = OriginalcompanyInfo.Postcode;
            PhoneBox.Text = OriginalcompanyInfo.Phone;
            BankAccountNumberBox.Text = OriginalcompanyInfo.BankAccountNumber;
        }

        private void OwnerNameBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.OwnerName != OwnerNameBox.Text)
            {
                OwnerNameBox.BackColor = Color.Red;
            }
        }

        private void OwnerSurnameBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.OwnerSurname != OwnerSurnameBox.Text)
            {
                OwnerSurnameBox.BackColor = Color.Red;
            }
        }

        private void CompanyBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.CompanyName != CompanyBox.Text)
            {
                CompanyBox.BackColor = Color.Red;
            }
        }

        private void ComapnyEmailBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.CompanyEmail != ComapnyEmailBox.Text)
            {
                ComapnyEmailBox.BackColor = Color.Red;
            }
        }

        private void StreetBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.Street != StreetBox.Text)
            {
                StreetBox.BackColor = Color.Red;
            }
        }

        private void CityBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.City != CityBox.Text)
            {
                CityBox.BackColor = Color.Red;
            }
        }

        private void PostCodeBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.Postcode != PostCodeBox.Text)
            {
                PostCodeBox.BackColor = Color.Red;
            }
        }

        private void PhoneBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.Phone != PhoneBox.Text)
            {
                PhoneBox.BackColor = Color.Red;
            }
        }

        private void BankAccountNumberBox_TextChanged(object sender, EventArgs e)
        {
            if (OriginalcompanyInfo.BankAccountNumber != BankAccountNumberBox.Text)
            {
                BankAccountNumberBox.BackColor = Color.Red;
            }
        }
    }
}
