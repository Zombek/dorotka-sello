﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DatabaseConnection.Factories;
using Operations;

namespace Dorotka___Sello.Controls
{
    public partial class PocztaControl : UserControl
    {
        public PocztaControl()
        {
            InitializeComponent();

        }

        protected override void OnLoad(EventArgs e)
        {
            var delivererFactory = new DelivererFactory();
            delivererBox.DataSource = delivererFactory.GetAllDeliverers();
            delivererBox.ValueMember = "dr_Id";
            delivererBox.DisplayMember = "dr_Name";
            informationLabel.Text = "Wybierz datę i dostawcę";
            base.OnLoad(e);
        }

        private void checkPackages_Click(object sender, EventArgs e)
        {

            informationLabel.Text = "Trwa sprawdzanie";

            try
            {
                CheckPocztaPolska check = new CheckPocztaPolska();
                var packages = new PackageFactory();
                var delivererId = Int16.Parse(delivererBox.SelectedValue.ToString());
                var ListOfPackages = packages.GetPackagesForDeliverer(delivererId, dateTimePicker.Value);
                List<string> packageNumbers = new List<string>();
                ListOfPackages.ForEach(pack => packageNumbers.Add(pack.pc_Document));

                bool ended = false;

                ended = check.CheckThesePackages(packageNumbers);
                if (ended)
                {
                    informationLabel.Text = $"Sprawdzanie zakończono - Wygenerowano plik w Moje Dokumenty";
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void delivererBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
