﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;
using DatabaseConnection;
using DatabaseConnection.Factories;
using Operations;

namespace Dorotka___Sello.Controls
{
    public partial class CsvControll : UserControl
    {
        private AutoCompleteStringCollection productNames = new AutoCompleteStringCollection();
        private DelivererFactory delivererFactory = new DelivererFactory();
        private ProductFactory productFactory = new ProductFactory();
        public CsvControll()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            setupDeliverers();
            setProductNames();

            base.OnLoad(e);
        }

        private void csvButton_Click(object sender, EventArgs e)
        {
            var deliverer = delivererBox.SelectedItem as sl_Deliverer;
            new CsvGenerator().GenerateCsvForCourier(deliverer.dr_Name, deliverer.dr_Id, GetCodeOfSelectedProduct(), dateTimePicker.Value);
        }


        private void setProductNames()
        {
            foreach (var item in productFactory.GetAllProductNamesWithCode())
            {
                productNames.Add(item);
            }

            productBox.DataSource = productNames;
            productBox.AutoCompleteCustomSource = productNames;
        }

        private void setupDeliverers()
        {
            var deliverers = delivererFactory.GetAllDeliverers();
            var x = new sl_Deliverer("--wybierz kuriera--", 0);
            deliverers.Insert(0, item: x);
            delivererBox.DataSource = deliverers;
            delivererBox.ValueMember = "dr_Id";
            delivererBox.DisplayMember = "dr_Name";
        }

        private int GetCodeOfSelectedProduct()
        {
            var product = productBox.SelectedItem.ToString();
            var startTrim = product.IndexOf("(")+1;
            var lastTrim = product.LastIndexOf(")");
            var toSearch = product.Substring(startTrim, lastTrim - startTrim);
            return productFactory.GetProductIdBySymbol(toSearch);
        }
    }
}
